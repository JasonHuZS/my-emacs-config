config=inits/01-user-config.el

watched=("$config")

changed_files=(`git diff "$frm_commit" "$to_commit" --name-only`)

function contains {
    for f in "${changed_files[@]}"; do
        if [[ $f = $1 ]]; then
            return 0
        fi
    done
    return 1
}

config_changed=false

for w in "${watched[@]}"; do
    if contains "$w"; then
        config_changed=true
        break
    fi
done
