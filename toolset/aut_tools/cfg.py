import collections


class Terminal(object):
    def __init__(self, sym):
        self._sym = sym

    @property
    def sym(self):
        return self._sym

    def __repr__(self):
        return "Terminal(sym={})".format(repr(self.sym))

    def __str__(self):
        return self.sym

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.sym == other.sym

    def __hash__(self):
        return 37 * hash(self.__class__) + hash(self.sym)


class NonTerminal(object):
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    def __repr__(self):
        return "NonTerminal(name={})".format(repr(self.name))

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.name == other.name

    def __hash__(self):
        return 37 * hash(self.__class__) + hash(self.name)


class Production(object):
    def __init__(self, lhs, rhs):
        self._lhs = lhs
        self._rhs = rhs

    @property
    def lhs(self):
        return self._lhs

    @property
    def rhs(self):
        return self._rhs

    def __repr__(self):
        return "Production(lhs={}, rhs={})".format(
            repr(self.lhs), repr(self.rhs))

    def __str__(self):
        return '{} -> {}'.format(self.lhs, ' '.join(str(r) for r in self.rhs))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            self.lhs == other.lhs and self.rhs == other.rhs


class Productions(object):
    def __init__(self, productions):
        self._productions = productions
        self._terms = set()
        self._nonterms = set()
        self._groups = collections.defaultdict(lambda: [])
        for p in self.productions:
            self._groups[p.lhs].append(p)
            for item in [p.lhs] + p.rhs:
                if isinstance(item, Terminal):
                    self._terms.add(item)
                else:
                    self._nonterms.add(item)

    @property
    def productions(self):
        return self._productions

    @property
    def groups(self):
        return self._groups

    @property
    def terms(self):
        return self._terms

    @property
    def nonterms(self):
        return self._nonterms

    def __getitem__(self, key):
        return self.groups[key]

    def __contains__(self, el):
        if isinstance(el, str):
            return el in self.groups
        if isinstance(el, Production):
            return self.idx(el) is not None
        raise Exception(
            "{} is not a recognized type for production containment.".format(
                el.__class__))

    def idx(self, prule):
        if prule.lhs not in self.groups:
            return None
        try:
            return self.groups[prule.lhs].index(prule)
        except ValueError:
            return None

    def __repr__(self):
        return "Productions(productions={})".format(repr(self.productions))

    def __str__(self):
        return '\n'.join(str(p) for p in self.productions)

    def _serialize(self):
        yield str(len(self.terms))
        for t in self.terms:
            yield str(t)

        yield str(len(self.nonterms))
        for nt in self.nonterms:
            yield str(nt)

        yield self.productions[0].lhs.name

        for p in self.productions:
            yield '{} {}'.format(str(p.lhs), ' '.join(str(r) for r in p.rhs))

    def serialize(self):
        return list(self._serialize())


class Derivation(object):
    def __init__(self, productions):
        self._productions = productions
        sup = self

        class Node(object):
            def __init__(self, nonterm, idx, children):
                self._nonterm = nonterm
                self._idx = idx
                self._children = children
                self._sup = sup

            @property
            def nonterm(self):
                return self._nonterm

            @property
            def idx(self):
                return self._idx

            @property
            def prule(self):
                return sup.productions[self.nonterm][self.idx]

            @property
            def children(self):
                return self._children

            def matched(self):
                acc = []
                i = 0
                for elem in self.prule:
                    if isinstance(elem, Terminal):
                        acc.append(elem.sym)
                    acc += self.children[i].matched()
                return acc

            def __repr__(self):
                return "Node(nonterm={}, idx={}, children={})".format(
                    repr(self.nonterm), repr(self.idx), repr(self.children))

        self._Node = Node

    @property
    def productions(self):
        return self._productions

    @property
    def Node(self):
        return self._Node

    def create_node(self, prule, children):
        i = self.productions.idx(prule)
        if i is None:
            raise Exception("production rule {} does not exist.".format(prule))
        return self.Node(prule.lhs, i, map(self.create_node, children))

    def __repr__(self):
        return "Derivation(productions={}, string={})".format(
            repr(self.productions), repr(self.string))


def split_lines(s):
    return map(lambda s: s + '\n', s.splitlines())


def parse_lines(lines):
    nonterm = set()
    prods = []
    for line in lines:
        line = line.strip()
        if not len(line):
            continue
        l, r = map(lambda s: s.strip(), line.split('->'))
        nonterm.add(l)
        prods.append(Production(l, r.split()))

    def tokenize(s):
        if s in nonterm:
            return NonTerminal(s)
        return Terminal(s)

    return Productions(map(lambda p: Production(tokenize(p.lhs), map(tokenize, p.rhs)),
                           prods))


def deserialize(lines):
    nterms = int(lines[0])
    terms = set()
    i = 1
    for _ in range(nterms):
        terms.add(lines[i].strip())
        i += 1

    nnonterms = int(lines[i])
    i += 1
    nonterms = set()
    for _ in range(nnonterms):
        nonterms.add(lines[i].strip())
        i += 1

    initial = lines[i].strip()
    i += 1

    nrules = int(lines[i])
    i += 1
    rules = []
    for _ in range(nrules):
        words = lines[i].strip().split()
        rules.add(Production(NonTerminal(words[0]),
                             ))
