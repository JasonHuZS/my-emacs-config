class AutConv(object):
    def __init__(self, aut):
        self.aut = aut
        self._states = set()
        self._transitions = []
        self._result = None

    @property
    def orig_transitions(self):
        return self.aut.transitions

    @property
    def orig_states(self):
        return self.aut.states

    @property
    def alphabet(self):
        return self.aut.alphabet

    @property
    def states(self):
        return frozenset(self._states)

    @property
    def transitions(self):
        return self._transitions

    @property
    def result(self):
        return self._result

    @property
    def finish(self):
        raise NotImplemented()

    def gen_states(self, states, word):
        raise NotImplemented()

    def rec_pred(self, ns):
        raise NotImplemented()

    def gen_initial(self):
        raise NotImplemented()

    def gen_automaton(self, alphabet, states, initial, finish, transitions):
        raise NotImplemented()

    def dfs(self, state):
        self._states.add(state)

        for word in self.alphabet:
            for ns in self.gen_states(state, word):
                if self.rec_pred(ns):
                    self.dfs(ns)

                self.transitions.append((state, word, ns))

    def convert(self):
        initial = self.gen_initial()
        self.dfs(initial)
        self._result = self.gen_automaton(self.alphabet, self._states, initial,
                                          self.finish,
                                          self.transitions)
