from aut import Nfa, epsilon
from aut_conv import AutConv


class EpsilonClosure(AutConv):
    def __init__(self, nfa):
        super(EpsilonClosure, self).__init__(nfa)
        self._closures = {}

    @property
    def finish(self):
        return self._states.intersection(self.aut.finish)

    def _closure(self, cur):
        n = {t for f, s, t in self.orig_transitions
             if f in cur
             if t not in cur
             if s == epsilon}
        if len(n):
            return self._closure(cur.union(n))
        return cur

    def closure(self, state):
        if state not in self._closures:
            self._closures[state] = self._closure(frozenset([state]))
        return self._closures[state]

    def gen_states(self, state, word):
        return {t for cs in self.closure(state)
                for ss in cs
                for f, s, t in self.orig_transitions
                if ss == f
                if s == word}

    def rec_pred(self, ns):
        return ns not in self._states

    def gen_initial(self):
        return self.aut.initial

    def gen_automaton(self, alphabet, states, initial, finish, transitions):
        return Nfa(alphabet, states, initial, finish, transitions)
