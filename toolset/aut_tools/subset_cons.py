from aut import Nfa, Dfa
from aut_conv import AutConv


class NfaToDfa(AutConv):
    def __init__(self, nfa):
        super(NfaToDfa, self).__init__(nfa)

    @property
    def finish(self):
        return frozenset([s for s in self._states if len(s.intersection(self.aut.finish))])

    def gen_states(self, states, word):
        return [frozenset([t for f, s, t in self.orig_transitions if f in states and word == s])]

    def gen_initial(self):
        return frozenset([self.aut.initial])

    def rec_pred(self, ns):
        return len(ns) and ns not in self._states

    def gen_automaton(self, alphabet, states, initial, finish, transitions):
        return Dfa(alphabet, states, initial, finish, transitions)


# class NfaToDfa(object):
#     def __init__(self, nfa):
#         self.nfa = nfa
#         self._states = set()
#         self._done = set()
#         self._transitions = []
#         self._result = None

#     @property
#     def alphabet(self):
#         return self.nfa.alphabet

#     @property
#     def ntransitions(self):
#         return self.nfa.transitions

#     @property
#     def transitions(self):
#         return self._transitions

#     def dfs(self, states):
#         self._states.add(states)
#         for a in self.alphabet:
#             ns = frozenset(
#                 [t for f, s, t in self.ntransitions if f in states and a == s])
#             if len(ns) and ns not in self._states:
#                 self.dfs(ns)

#             self._transitions.append((states, a, ns))

#         self._done.add(states)

#     def convert(self):
#         initial = frozenset([self.nfa.initial])
#         self.dfs(initial)
#         self._result = Dfa(self.alphabet, self._states, initial,
#                            {s for s in self._states if len(
#                                s.intersect(self.nfa.finish))},
#                            self.transitions)
