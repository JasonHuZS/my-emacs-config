import re
import sys
import string
from collections import namedtuple, defaultdict

__doc__ = """
This is a small domain specific language which can be used to describe NFA and DFA

syntax are following:

alphabet = { @key : { @key | word, ... }, ... }
initial = state
finish = { state, ... }
states = { state, ... }
transitions =
(state | { state, ... }) '('word | @key | _ '|' ...')' -> (state | { state, ... }) ...

all parenthesis and braces can be broken into multiline.

essential all characters can be used to name @key, word, or state, as long as
proper escape is applied. these three components live in different namespace
so the program will not confuse if the same name is used multiple times.


semantics goes following:

alphabet specifies the alphabet of the automaton, one can specify a word,
or group words under some @key. curly braces({}), comma(,), backslash(\)
and at(@) are special chars, and all can be escaped by backslash. colon(:)
itself has no special meaning.

all words in @key definition will be flattened out into alphabet, and
@key's can be used to define a new @key. however, a later appearence of
@key can only refer to @key's that are defined earlier, and a duplicated
@key definition is not allowed; while it's OK to duplicate word in alphabet
multiple time as needed, if it belongs to different @key definitions.

@ itself is a special @key, which refers to the whole alphabet. however,
it makes no sense to use it in alphabet definition, while it does help
sometimes when defining transitions(see below).


initial specifies the initial state of the automaton.

finish specifies the accepting states

states specifies the set of all states. this field is optional since it will
be derived from transitions(see below).

transitions specifies the transition relation. a transition relation can be
a state takes one or more words and transit to another state or states. left
to the arrow is from state and right to it is to state(s). what's between
 parenthesis is treated as input of from state. if a pair of from and
to states can be described by various words in the alphabet, one can separate
them with pipe(|).

inside of parenthesis, following characters are special: parenthesis(),
pipe(|), backslash(\), at(@) and underscore(_). similarly then can be escaped
by backslash when necessary. @key and word have the same meaning as they do in
alphabet, underscore is a convenient way of saying "all remaining words that
have not been used to describe from state at this point", which can be indeed
considered as wildcard.


sanity checking:

the kind of autmaton is distinguished by file suffix, .dfa-desc or .nfa-desc.

after parsing the description file, the program will perform sanity checking,
based on which kind of automaton the file is descibing, NFA or DFA.

NFA sanity check assumes epsilon-NFA by default, and therefore it's
required not to mention epsilon in the alphabet; while epsilon has no special
meaning for DFA.

In general, it checks following things:

1. required fields are defined.
2. there is transition from initial state.
3. all finish states are reachable.
4. all words used in transitions are defined in alphabet.

in addition, DFA is also checked to make sure it's indeed an DFA.

output:

final output of this file as executable dump out an automaton description
format as specified at University of Waterloo.


example:

following DFA describe decimal numbers < 100, with no useless leading zeros:

alphabet    = { 0, @nonzero : {1, 2, 3, 4, 5, 6, 7, 8, 9} }
initial     = start
finish      = {done}
transitions = start(0) -> done
              start(@nonzero) -> onedigit
              onedigit(_) -> done

very generous DFA:

alphabet    = { 0, 1 }
initial     = start
finish      = { start }
transitions = start(@) -> start

following NFA describes binary strings with 1010 as a substring

alphabet    = {0, 1}
initial     = start
finish      = {1010}
transitions = start(1) -> {1, start}
              start(0) -> start
              1(0) -> 10
              10(1) -> 101
              101(0) -> 1010
              1010(_) -> 1010

the same problem with (boring) epsilon transitions:

alphabet    = {0, 1}
initial     = start
finish      = {1010}
transitions = start(1) -> {1, start}
              start(0) -> start
              1(epsilon) -> 1'
              1'(0) -> 10
              10(epsilon) -> 10'
              10'(1) -> 101
              101(epsilon) -> 101'
              101'(0) -> 1010
              1010(_) -> 1010

at last, following DFA can tokenize (subset of) this language:

alphabet    = { @lower: {a, b, c, d, e, f, g, h, i, j, k, l, m,
                         n, o, p, q, r, s, t, u, v, w, x, y, z},
                @higher: {A, B, C, D, E, F, G, H, I, J, K, L, M,
                          N, O, P, Q, R, S, T, U, V, W, X, Y, Z},
                @letter: {@lower, @higher}
                @nonzero: {1, 2, 3, 4, 5, 6, 7, 8, 9}
                @num: {0, @nonzero},
                @alphnum: {@letter, @num}
                @lp: { \( }, @rp: { \) },
                @lb: { \{ }, @rb: { \} },
                @spc: { \  }, @nl: { \
                }, \\, _, |, \@,
                @nonspecial: {@alphnum, +, -, *,  /, <,
                              >, %, ;, !, \,, :, = },
                @id: {@nonspecial, _, |, \@ } }
initial     = start
finish      = { lb, rb, lp, rp, colon, comma, pipe,
               eq, arrow, uscore, id }
transitions = start(@lb) -> lb
              start(@rb) -> rb
              start(@lp) -> lp
              start(@rp) -> rp

              start(:) -> colon
              start(,) -> comma
              start(\|) -> pipe

              start(=) -> eq

              start(-) -> dash
              dash(>) -> arrow

              start(\_) -> uscore

              start(\@) -> at

              id(\\) -> allchar
              id(, | @lp | @rp | @lb | @rb) -> wrong
              id(_) -> id
              allchar(_) -> id

              {at, colon, pipe, eq,
               dash, arrow, uscore}(@id) -> id
"""

defline = re.compile(r"^\s*(\w+)\s*=\s*(.*)")

epsilon = "epsilon"

Dfa = namedtuple("Dfa", "alphabet, states, initial, finish, transitions")

Nfa = namedtuple("Nfa", "alphabet, states, initial, finish, transitions")


def states_str(states):
    if len(states) == 1:
        return next(iter(states))
    else:
        return '{{{}}}'.format(', '.join(states))


def uniq(l):
    s = set()
    for e in l:
        if e not in s:
            s.add(e)
            yield e


def consume_spaces(scanner):
    for c in scanner:
        if not c.isspace():
            break

    if scanner.done:
        return
    scanner.push_back(c)


def consume(scanner, sep, escape, stops, keep=False):
    acc = []

    def inpc(ch):
        if ch in sep:
            return 'sep'
        elif ch in stops:
            scanner.push_back(ch)
            return 'stops'
        elif ch == escape:
            try:
                if keep:
                    acc.append(ch)
                ch = next(scanner)
                acc.append(ch)
            except StopIteration:
                raise Exception(
                    "{} should be followed by something.".format(escape))
        else:
            acc.append(ch)
        return 'word'

    ch = next(scanner)
    if ch in stops:
        return 'stops', ch
    if inpc(ch) == 'sep':
        return 'sep', ''

    try:
        while True:
            char = next(scanner)
            if inpc(char) in ('sep', 'stops'):
                break
    except StopIteration:
        pass
    return 'word', ''.join(acc)


def unescape(word, escape='\\'):
    return re.sub(re.escape(escape) + r'(.)', r'\1', word, flags=re.S)


def stripall(l):
    return map(lambda s: s.strip(), l)


class StringsScanner(object):
    def __init__(self, lines):
        self._lines = lines
        self.reset()

    def reset(self):
        def loop():
            for i in range(len(self._lines)):
                l = self._lines[i]
                self._lc = i

                for j in range(len(l)):
                    self._col = j

                    yield l[j]

        self._lc = 0
        self._col = 0
        self._done = False

        self._iterator = loop()

    @property
    def counter(self):
        return self._lc, self._col

    @property
    def lines(self):
        return self._lines

    @property
    def done(self):
        return self._done

    def push_back(self, w):
        orig = self._iterator

        def loop():
            for c in w:
                yield c
            for c in orig:
                yield c

        self._iterator = loop()

    def __iter__(self):
        return self

    def next(self):
        try:
            return next(self._iterator)
        except StopIteration:
            self._done = True
            raise

    def peak(self):
        c = next(self)
        self.push_back(c)
        return c

    def consume_spaces(self):
        consume_spaces(self)

    def consume(self, sep=',', escape='\\', stops='{}', keep=False):
        self.consume_spaces()
        return consume(self, sep=sep, escape=escape,
                       stops=stops + string.whitespace, keep=keep)

    def consume_util(self, ws):
        try:
            while True:
                char = next(self)
                if char in ws:
                    break
        except StopIteration:
            pass


def scan_csv(scanner, sep=',', escape='\\', brackets='{}', keep=False):
    lb = brackets[0]
    rb = brackets[1]
    words = []

    flg, char = scanner.consume(sep, escape, brackets, keep)
    if not (flg == 'stops' and char == lb):
        raise Exception('must start with open bracket.')

    try:
        while True:
            flg, word = scanner.consume(sep, escape, brackets, keep)

            if flg == 'sep':
                continue
            elif flg == 'stops' and word == lb:
                raise Exception('nested brackets.')
            elif flg == 'stops' and word == rb:
                break

            words.append(word)

    except StopIteration:
        return False, words

    return True, words


def sanitize_nfa(result, allow_epsilon=True):
    del result['env']
    must_has = ('alphabet', 'finish', 'initial', 'transitions')

    for f in must_has:
        if f not in result:
            raise Exception(
                "there must be {} specified in the source file".format(f))

    if allow_epsilon and epsilon in result['alphabet']:
        raise Exception(
            "epsilon has special meaning; it cannot be a part of the alphabet.")

    if 'states' not in result:
        result['states'] = set()

    can_start = False
    can_end = False

    for f, s, t in result['transitions']:
        result['states'].add(f)
        result['states'].add(t)
        if f == result['initial']:
            can_start = True
        if t in result['finish']:
            can_end = True
        if not (allow_epsilon and s == epsilon) \
           and s not in result['alphabet']:
            raise Exception(
                '{} is not in alphabet in this transition: {}({}) -> {}.'
                .format(s, f, s, t))

    if not can_start:
        raise Exception(
            "no transitions from initial state {}.".format(result['initial']))

    if not can_end:
        raise Exception("no transitions to any finish states: {}".format(
            ', '.join(result['finish'])))

    if len(result['finish'] - result['states']):
        raise Exception("there are unreacheable finish states: {}".format(
            ', '.join(result['finish'] - result['states'])))

    result['transitions'] = list(uniq(result['transitions']))
    return Nfa(**result)


def sanitize_dfa(result):
    sanitize_nfa(result, False)

    m = defaultdict(lambda: [])
    for f, s, t in result['transitions']:
        m[f, s].append(t)

    wrong = [(f, s, ts) for (f, s), ts in m.items() if len(ts) != 1]
    if len(wrong):
        raise Exception(
            "Following transitions are not deterministic: {}".format(wrong))
    return Dfa(**result)


def parse_alphabet(scanner):
    """
    following syntax is expected:

    { @key : { word | @key, ... }, ... }
    """
    brackets = '{}'
    lb = brackets[0]
    rb = brackets[1]
    alphabet = {}
    words = set()

    flg, char = scanner.consume()
    if not (flg == 'stops' and char == lb):
        raise Exception('must start with open bracket.')

    def translate(wos):
        r = set()
        for wo in wos:
            if wo.startswith('@'):
                if wo not in alphabet and wo != '@':
                    raise Exception(
                        "{} is not defined in alphabet yet.".format(wo))
                if wo == '@':
                    raise Exception(
                        "@ means the whole alphabet, which cannot " +
                        "be refered to while defining one.")
                r.update(alphabet[wo])
            else:
                r.add(unescape(wo))
        return r

    while True:
        scanner.consume_spaces()
        c = next(scanner)
        scanner.push_back(c)
        if c == '@':
            _, w = scanner.consume(sep=':')
            if w in alphabet:
                raise Exception("{} has been defined in alphabet.".format(w))
            if w == '@':
                raise Exception(
                    "@ means the whole alphabet, cannot be redefined.")

            flg, nx = scanner.consume(sep=':', keep=True)
            if flg == 'stops':
                scanner.push_back(nx)

            done, fields = scan_csv(scanner, keep=True)
            if not done:
                raise Exception(
                    "field definition {} failed to terminate.".format(w))
            alphabet[w] = translate(fields)
            words.update(alphabet[w])
        else:
            flg, w = scanner.consume()
            if flg == 'stops' and w == lb:
                raise Exception("nested brackets.")
            elif flg == 'stops' and w == rb:
                break
            elif flg == 'sep':
                continue
            words.add(w)

    return words, alphabet


def parse_transitions(scanner):
    """
    transitions has following syntax:

    (state | { state, ... }) '('word | @key | _ '|' ...')' -> (state | { state, ... })
    """
    paren = '()'
    braces = '{}'
    lp, rp = paren
    sep = '|'

    flg, frms = scanner.consume(stops=paren + braces)
    if flg == 'stops':
        if frms in paren:
            raise Exception(
                "transition does not seem to have a from state: {}"
                .format(scanner.lines[scanner.counter[0]]))
        scanner.push_back(frms)
        done, frms = scan_csv(scanner)
        if not done:
            raise Exception(
                "braces of from states are not closed: {}"
                .format(scanner.lines[scanner.counter[0]]))
    else:
        frms = [frms]

    done, words = scan_csv(scanner, sep=sep, brackets=paren, keep=True)
    if not done:
        raise Exception(
            "parenthesis failed to be enclosed from states" +
            " {} recieving following inputs: {}".format(
                states_str(frms), words))

    _, arrow = scanner.consume(sep=string.whitespace)
    assert arrow == '->'

    flg, nx = scanner.consume()
    if flg == 'stops':
        scanner.push_back(nx)
        done, tos = scan_csv(scanner)
        if not done:
            raise Exception(
                "braces of to states are not closed of following " +
                "(partial) transition: {}({}) -> {}"
                .format(states_str(frms),
                        ' | '.join(words),
                        states_str(tos)))

        return frms, words, tos
    return frms, words, [nx]


def parse_string(lines, sanitizer):
    result = {'env': {}}

    def add_transitions(tup):
        if 'transitions' not in result:
            result['transitions'] = []
        fs, ss, ts = tup
        results = []

        def do_add(w):
            for f in fs:
                for t in ts:
                    results.append((f, w, t))

        for s in ss:
            if s.startswith('@'):
                if s not in result['env'] and s != '@':
                    raise Exception(
                        "{} is not in the env for transition {}({}) -> {}"
                        .format(s, states_str(fs),
                                ' | '.join(ss),
                                states_str(ts)))
                for w in result['alphabet'] if s == '@' else result['env'][s]:
                    do_add(w)
            elif '_' == s:
                for ms in set(result['alphabet']) - \
                        {s for fp, s, _ in result['transitions'] if fp in fs}:
                    do_add(ms)
            else:
                do_add(unescape(s))

        result['transitions'] += results

    i = 0
    mode = 'normal'

    def finalize_scanner(scanner):
        lc, col = scanner.counter
        s = scanner.lines[lc][col + 1:]
        if len(s.strip()):
            raise Exception(
                'following line has nonempty trailing which failed to parse: {}'
                .format(lines[i + lc]))
        return i + lc + 1

    while i < len(lines):
        line = lines[i].strip()
        if not len(line):
            i += 1
            continue

        if mode == 'normal':
            m = defline.match(line)
            key, value = stripall([m.group(1), m.group(2)])

            if key == 'alphabet':
                scanner = StringsScanner([value + '\n'] + lines[i + 1:])
                words, env = parse_alphabet(scanner)

                i = finalize_scanner(scanner)
                result[key] = words
                result['env'] = env

            elif key in ('states', 'finish'):
                scanner = StringsScanner([value + '\n'] + lines[i + 1:])
                done, fields = scan_csv(scanner)
                if not done:
                    raise Exception(
                        "key {} failed to have enclosing brackets: {}"
                        .format(key, value))
                result[key] = set(fields)
                i = finalize_scanner(scanner)
            elif key == 'initial':
                scanner = StringsScanner([value])
                _, result[key] = scanner.consume()
                finalize_scanner(scanner)
                i += 1
            elif key == 'transitions':
                if len(value):
                    scanner = StringsScanner([value + '\n'] + lines[i + 1:])
                    r = parse_transitions(scanner)
                    i = finalize_scanner(scanner)
                    try:
                        add_transitions(r)
                    except AssertionError:
                        raise Exception(
                            "transition format is not right: {}".format(value))
                    mode = 'transitions'
                else:
                    i += 1
            else:
                raise Exception(
                    "{} as key is not recognized.".format(key))
        else:
            if defline.match(line) is None:
                scanner = StringsScanner(lines[i:])
                r = parse_transitions(scanner)
                i = finalize_scanner(scanner)
                try:
                    add_transitions(r)
                except AssertionError:
                    raise Exception(
                        "transition format is not right: {}".format(value))

            else:
                mode = 'normal'

    return sanitizer(result)


def parse_nfa(lines):
    return parse_string(lines, sanitize_nfa)


def parse_dfa(lines):
    return parse_string(lines, sanitize_dfa)


def parse_nfa_str(s):
    return parse_nfa(map(lambda s: s + '\n', s.splitlines()))


def parse_dfa_str(s):
    return parse_dfa(map(lambda s: s + '\n', s.splitlines()))


def parse_file(f):
    with open(f) as fd:
        lines = list(fd)
        if f.endswith(".dfa-desc"):
            return parse_dfa(lines)
        elif f.endswith(".nfa-desc"):
            return parse_nfa(lines)
        else:
            raise Exception("Unknown file type: {}".format(f))


def print_out(fp, result):
    alph = result.alphabet
    print >>fp, len(alph)
    for a in alph:
        print >>fp, a

    states = result.states
    print >>fp, len(states)
    for s in states:
        print >>fp, s

    print >>fp, result.initial

    finish = result.finish
    print >>fp, len(finish)
    for f in finish:
        print >>fp, f

    trans = result.transitions
    print >>fp, len(trans)
    for f, s, t in trans:
        print >>fp, "{} {} {}".format(f, s, t)


def main():
    f = sys.argv[1]
    result = parse_file(f)
    if len(sys.argv) == 2:
        print_out(sys.stdout, result)
    else:
        with open(sys.argv[2], 'w') as fd:
            print_out(fd, result)


if __name__ == '__main__':
    main()
