import ast

repl_ident = ast.Literal('user-config-replacement')
env_ident = ast.Literal('env-identifier')

defconst = ast.Literal("defconst")
defun = ast.Literal("defun")

lst = ast.Literal('list')
nil = ast.Literal('nil')
true = ast.Literal('t')


quote = ast.Literal('quote')
