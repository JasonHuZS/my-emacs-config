#!/usr/bin/env python3

import sys
import ast
from literals import repl_ident
import utils


def remove_config(toks):
    for tok in utils.find_concrete(toks):
        tok.mark_modified()
        tok[2] = ast.QuotedTree("'", repl_ident)


if __name__ == '__main__':
    config = sys.stdin.read()
    lisp_toks = ast.parse_lisp_inp(config)
    remove_config(lisp_toks)
    print(ast.to_elisp(lisp_toks))

    sys.exit(0)
