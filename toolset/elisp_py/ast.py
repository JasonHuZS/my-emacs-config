import string
import parser
from io import StringIO
from contextlib import closing


def indent_str(indent):
    return ' ' * indent


def surround(s, paren):
    op, cl = paren
    return '{}{}{}'.format(op, s, cl)


class LispTree(object):

    def __init__(self, subtrees, src='', paren='()', modified=False):
        self._subtrees = subtrees
        self._paren = paren
        self._src = src
        if not len(src):
            self._modified = True
        else:
            self._modified = modified

    @property
    def src(self):
        return self._src

    @property
    def modified(self):
        return self._modified

    @property
    def prefixed(self):
        return False

    @property
    def paren(self):
        return self._paren

    def head(self):
        return self._subtrees[0]

    def __iter__(self):
        return iter(self._subtrees)

    def __len__(self):
        return len(self._subtrees)

    def __getitem__(self, key):
        return self._subtrees[key]

    def __setitem__(self, key, val):
        self._modified = True
        self._subtrees[key] = val

    def __delitem__(self, key):
        self._modified = True
        del self._subtrees[key]

    def mark_modified(self):
        self._modified = True
        for t in self._subtrees:
            t.mark_modified()

    def prettify(self, indent=0, deep=True, skip_comment=True):
        if deep:
            def to_str(it, indent):
                return it.prettify(indent, deep, skip_comment)
        else:
            def to_str(it, indent):
                return it.as_code(indent)

        def wrap_up(s, end_comment):
            return indent_str(indent) \
                + surround(s.lstrip() if end_comment else s.strip(),
                           self.paren)

        trees = filter(lambda t: not isinstance(t, Comment), self._subtrees) \
            if skip_comment else self._subtrees

        if not len(trees):
            return indent_str(indent) + self.paren

        with closing(StringIO()) as io:
            i = 0
            while i < len(trees):
                t = trees[i]
                if not isinstance(t, Comment):
                    break
                io.write(to_str(t, indent + 1))
                io.write('\n')
                i += 1

            if i >= len(trees):
                return wrap_up(io.getvalue(), True)

            h = to_str(trees[i], indent + 1)
            io.write(h)

            def conseq_literal(t):
                return isinstance(t, Literal) \
                    or isinstance(t, PrefixedTree) and conseq_literal(t.tree)

            if i + 1 < len(trees) and\
               conseq_literal(trees[i]) and not isinstance(trees[i + 1], Comment):
                trm_indent = 2 + indent + len(h.strip())
                h2 = to_str(trees[i + 1], trm_indent)
                io.write(' ')
                io.write(h2.strip())
                io.write('\n')
                i += 2
            else:
                trm_indent = indent + 1
                io.write('\n')
                i += 1

            while i < len(trees):
                t = trees[i]
                if isinstance(t, Comment):
                    io.write(to_str(t, indent + 1))
                else:
                    io.write(to_str(t, trm_indent))
                io.write('\n')
                i += 1
            return wrap_up(io.getvalue(), isinstance(trees[-1], Comment))

    def as_code(self, indent=0):
        if self.modified:
            return self.prettify(indent, deep=True, skip_comment=False)
        else:
            return self.src if indent == 0 \
                else '\n'.join(indent_str(indent) + l
                               for l in self.src.splitlines())

    def __repr__(self):
        return "LispTree(subtrees={}, src={}, paren={})"\
            .format(repr(self._subtrees), repr(self.src), repr(self.paren))

    def __str__(self):
        return self.as_code()

    def __eq__(self, other):
        return type(self) == type(other) \
            and self._subtrees == other._subtrees \
            and self.src == other.src \
            and self.modified == other.modified


class Literal(LispTree):

    def __init__(self, literal):
        self._literal = literal

    @property
    def src(self):
        return self._literal

    @property
    def modified(self):
        return True

    @property
    def prefixed(self):
        return False

    def head(self):
        return self._literal

    def mark_modified(self):
        pass

    def __iter__(self):
        return iter([self])

    def __len__(self):
        return 1

    def __getitem__(self, key):
        if key == 0:
            return self._literal
        raise IndexError(key)

    def __setitem__(self, key, val):
        pass

    def prettify(self, indent=0, deep=True, skip_comment=True):
        return indent_str(indent) + self._literal

    def __repr__(self):
        return 'Literal(literal={})'.format(repr(self._literal))

    def __eq__(self, other):
        return type(self) == type(other) \
            and self._literal == other._literal


class Comment(LispTree):
    def __init__(self, comment):
        self._comment = comment.strip()

    @property
    def comment(self):
        return self._comment

    @property
    def src(self):
        return self.comment

    @property
    def modified(self):
        return True

    @property
    def prefixed(self):
        return False

    def head(self):
        return self.comment

    def mark_modified(self):
        pass

    def __iter__(self):
        return iter([self])

    def __len__(self):
        return 1

    def __getitem__(self, key):
        if key == 0:
            return self._literal
        raise IndexError(key)

    def __setitem__(self, key, val):
        pass

    def prettify(self, indent=0, deep=True, skip_comment=True):
        if skip_comment:
            return ''
        return indent_str(indent) + self.comment

    def __repr__(self):
        return "Comment(comment={})".format(repr(self.comment))

    def __eq__(self, other):
        return type(self) == type(other) and self.comment == other.comment


class PrefixedTree(LispTree):

    def __init__(self, prefix, tree):
        self._prefix = prefix
        self._tree = tree

    @property
    def prefix(self):
        return self._prefix

    @property
    def tree(self):
        return self._tree

    @property
    def src(self):
        return self.prefix + self.tree.src

    @property
    def modified(self):
        return self.tree.modified

    @property
    def prefixed(self):
        return True

    def mark_modified(self):
        self.tree.mark_modified()

    def head(self):
        return self.tree.head()

    def __iter__(self):
        return iter(self.tree)

    def __len__(self):
        return len(self.tree)

    def __getitem__(self, key):
        return self.tree[key]

    def __setitem__(self, key, val):
        self.tree[key] = val

    def prettify(self, indent=0, deep=True, skip_comment=True):
        return indent_str(indent) + self.prefix \
            + self.tree.prettify(indent + len(self.prefix), deep).strip()

    def __repr__(self):
        return "PrefixdTree(prefix={}, tree={})"\
            .format(repr(self.prefix), repr(self.tree))

    def __eq__(self, other):
        return type(self) == type(other) \
            and self.prefix == other.prefix \
            and self.tree == other.tree


class QuotedTree(PrefixedTree):

    def __init__(self, quote, tree):
        super(QuotedTree, self).__init__(quote, tree)

    @property
    def quote(self):
        return self.prefix

    @property
    def _quote_literal(self):
        if self.quote == "'":
            return 'quote'
        elif self.quote == '`':
            return 'backquote'
        else:
            raise Exception()

    def explicitly(self):
        return LispTree([Literal(self._quote_literal), self.tree])

    def __repr__(self):
        return "QuotedTree(quote={}, tree={})"\
            .format(repr(self.prefix), repr(self.tree))


class UnquotedTree(PrefixedTree):
    def __init__(self, tree):
        super(UnquotedTree, self).__init__(',', tree)

    def __repr__(self):
        return "UnquotedTree(tree={})".format(repr(self.tree))


class SpliceTree(PrefixedTree):
    def __init__(self, tree):
        super(SpliceTree, self).__init__(',@', tree)

    def __repr__(self):
        return "SpliceTree(tree={})".format(repr(self.tree))


def parse_lisp_tok(scanner):
    op, cl = '()'
    ob, cb = '[]'
    c = scanner.peak()
    if c in string.whitespace:
        scanner.skip_all(string.whitespace)
        return parse_lisp_tok(scanner)
    if c == '"':
        return Literal(scanner.consume_enclosed())
    if c == op:
        return LispTree(*scanner.consume_separated(
            parse_lisp_tok, '()',
            lambda s: s.skip_all(string.whitespace)))
    if c == ob:
        return LispTree(*scanner.consume_separated(
            parse_lisp_tok, '[]',
            lambda s: s.skip_all(string.whitespace)))
    if c in cl + cb:
        raise Exception("unmatching parenthesis.")
    if c in "'`":
        next(scanner)
        return QuotedTree(c, parse_lisp_tok(scanner))
    if c == ',':
        next(scanner)
        if scanner.peak() == '@':
            next(scanner)
            return SpliceTree(parse_lisp_tok(scanner))
        return UnquotedTree(parse_lisp_tok(scanner))
    if c == ';':
        comm = scanner.consume_until('\n')
        if scanner.peak() == '\n':
            scanner.pop_off(1)
        return Comment(comm)
    return Literal(
        scanner.consume_until(stop=string.whitespace + op + cl + ob + cb + "'`;,",
                              skip_first=''))


def parse_lisp_inp(inp):
    scanner = parser.Scanner(inp)

    acc = []
    scanner.skip_all(string.whitespace)
    while not scanner.done:
        acc.append(parse_lisp_tok(scanner))
        scanner.skip_all(string.whitespace)

    return acc


def to_elisp(structs):
    return '\n\n'.join(struct.as_code() for struct in structs)
