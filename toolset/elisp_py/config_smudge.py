#!/usr/bin/env python3

import json
import sys
import ast
import utils
from literals import env_ident, nil


def modify_lisp(toks, config):
    missed = []
    for tok in utils.find_templates(toks):
        if tok[1].src in config:
            tok[2] = ast.LispTree([env_ident,
                                      utils.to_lisp_tok(config[tok[1].src])],
                                     '', modified=True)
        else:
            tok[2] = ast.LispTree([env_ident,
                                      nil],
                                     '', modified=True)
            missed.append(tok[1].src)
    return missed


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Need a JSON config file.", file=sys.stderr)
        sys.exit(1)

    json_file = sys.argv[1]
    print("Configurate using json file: {}".format(json_file), file=sys.stderr)

    try:
        with open(json_file) as fd:
            obj = json.load(fd)
    except ValueError as e:
        print("Wrong file format: {}".format(e), file=sys.stderr)
        sys.exit(1)

    config = sys.stdin.read()

    lisp_toks = ast.parse_lisp_inp(config)
    missed = modify_lisp(lisp_toks, obj)

    print(ast.to_elisp(lisp_toks))

    if len(missed):
        print("There are new fields in the lisp template which are not contained" \
            + " in the json config file: {}".format(
                ', '.join(map(str, missed))), file=sys.stderr)
        print("fields will be added to the json config automatically with default value null.", file=sys.stderr)

        for m in missed:
            obj[m] = None

        with open(json_file, 'w') as fd:
            json.dump(obj, fd, indent=4, sort_keys=True)

    sys.exit(0)
