import string


class Scanner(object):

    def __init__(self, inp):
        if isinstance(inp, list):
            self._lines = inp
            self._raw_text = ''.join(self._lines)
        else:
            self._raw_text = inp
            self._lines = list(map(lambda s: s + '\n', inp.splitlines()))
        self.reset()

    def reset(self):
        self._row = 0
        self._col = 0
        self._done = False

    @property
    def raw_text(self):
        return self._raw_text

    @property
    def lines(self):
        return self._lines

    @property
    def row(self):
        return self._row

    @property
    def col(self):
        return self._col

    @property
    def pos(self):
        return self.col + sum(len(self.lines[i]) for i in range(self.row))

    @property
    def done(self):
        return self._done

    @property
    def coord(self):
        return self.row, self.col

    def crop(self, srow, scol, erow, ecol):
        spos = self.coord_to_pos((srow, scol))
        epos = self.coord_to_pos((erow, ecol))
        return self.raw_text[spos:epos]

    def __iter__(self):
        return self

    def __getitem__(self, key):
        if isinstance(key, int):
            row, col = self.pos_to_coord(key)
            return self.lines[row][col]
        if isinstance(key, tuple):
            row, col = key
            return self.lines[row][col]
        if isinstance(key, slice):
            if key.start is None:
                spos = self.pos
            elif isinstance(key.start, int):
                spos = key.start
            else:
                spos = self.coord_to_pos(key.start)
            if key.stop is None:
                with self.attempt(always_rollback=True):
                    self._mark_done()
                    epos = self.pos
            elif isinstance(key.stop, int):
                epos = key.stop
            else:
                epos = self.coord_to_pos(key.stop)
            step = key.step if key.step is not None else 1

            return self.raw_text[spos:epos:step]

    @property
    def _curchar(self):
        return self.lines[self.row][self.col]

    @property
    def curline(self):
        return self.lines[self.row]

    def next(self):
        if self.done:
            raise StopIteration()
        c = self._curchar
        self.pop_off(1)
        return c

    def __next__(self):
        return self.next()

    def _mark_done(self):
        self._done = True
        self._row = len(self.lines) - 1
        self._col = len(self.curline)

    def pop_off(self, n):
        " return a boolean to indicate if the top is hit. x"
        while not self.done:
            self._col += n
            if self._col < len(self.curline):
                return False
            n = self._col - len(self.curline)
            self._col = 0
            self._row += 1
            if self._row >= len(self.lines):
                self._mark_done()
        else:
            return True

    def push_back(self, n):
        " return a boolean to indicate if the bottom is hit. "
        if self.done:
            self._mark_done()
            self._done = False
            return self.push_back(n)
        self._col -= n
        while self._col < 0:
            n = -self._col
            self._row -= 1
            if self._row == -1:
                self.reset()
                return True
            self._col = len(self.curline) - n
        else:
            return False

    def peak(self):
        c = next(self)
        self.push_back(1)
        return c

    def skip_all(self, s):
        for c in self:
            if c not in s:
                self.push_back(1)
                break

    def expect_next(self, s):
        return self.raw_text.startswith(s, self.pos)

    def expect_until(self, ok, stop):
        for c in self:
            if c in ok:
                pass
            elif c in stop:
                break
            else:
                raise Exception("unexpected char {} from line: {}"
                                .format(c, self.curline))

    def consume_until(self, stop, skip_first=string.whitespace):
        self.skip_all(skip_first)
        acc = []
        for c in self:
            if c in stop:
                self.push_back(1)
                break
            acc.append(c)
        return ''.join(acc)

    def consume_enclosed(self, oc='""', escape='\\', stop=''):
        op, cl = oc
        c = next(self)
        assert c == op

        acc = [c]

        c = next(self)
        while not self.done:
            if c in escape:
                acc.append(c)
                acc.append(next(self))
            elif op != cl and c == op:
                raise Exception("unmatching both ends of {} until {}: {}"
                                .format(oc, ''.join(acc), self.curline))
            elif c == cl:
                acc.append(cl)
                break
            elif c in stop:
                raise Exception(
                    "force stopped without matching both ends of {} until {}: {}"
                    .format(oc, ''.join(acc), self.curline))
            else:
                acc.append(c)
            c = next(self)
        else:
            raise Exception("unmatching both ends of {} until {}: {}"
                            .format(oc, ''.join(acc), self.curline))

        return ''.join(acc)

    def consume_separated(self, consumer, oc, skip):
        op, cl = oc
        row, col = self.row, self.col
        c = next(self)
        assert c == op

        acc = []
        skip(self)
        while not self.done:
            c = self.peak()
            if c == cl:
                next(self)
                break
            else:
                acc.append(consumer(self))
            skip(self)
        else:
            raise Exception("unmatching both ends of {} until {}: {}"
                            .format(oc,
                                    self.crop(row, col, self.row, self.col),
                                    self.curline))

        erow, ecol = self.row, self.col
        return acc, self.crop(row, col, erow, ecol)

    def pos_to_coord(self, pos):
        if pos >= 0:
            with self.attempt(always_rollback=True):
                self.reset()
                self.pop_off(pos)
                return self.coord
        else:
            with self.attempt(always_rollback=True):
                self._mark_done()
                self.push_back(abs(pos))
                return self.coord

    def coord_to_pos(self, coord):
        with self.attempt(always_rollback=True):
            self._row, self._col = coord
            return self.pos

    def attempt(self, suppress=None, always_rollback=False):
        """
        create a context of trying to perform some action. call rollback() of
        the context handler to manually rollback:

        with scanner.attempt() as handler:
            scanner.do_something()
            handler.rollback()
            will_not_execute()
        assert handler.rolledback == True

        control flow will stop at the point of rolling back, and the progress
        in scanner will be reverted. rollback will also be performed when any
        uncaught exception is thrown.

        context can be nested and each context will only catch its own rollback
        signal.
        """
        return RollbackContext(self, suppress, always_rollback)


class RollbackContext(object):
    def __init__(self, scanner, suppress=None, always_rollback=False):
        self._scanner = scanner
        self._suppress = suppress if suppress is not None else []
        self._always_rollback = always_rollback
        self._rolledback = False

        class RollBack(Exception):
            pass

        # a unique signal of rolling back
        self._rollback = RollBack()

    @property
    def rolledback(self):
        return self._rolledback

    def __enter__(self):
        self._row, self._col = self._scanner.coord
        self._done = self._scanner.done
        return self

    def rollback(self):
        raise self._rollback

    def __exit__(self, exec_type, exec_value, traceback):
        if self._always_rollback or exec_value is not None:
            self._scanner._row = self._row
            self._scanner._col = self._col
            self._scanner._done = self._done
            self._rolledback = True

        if exec_type in self._suppress or self._rollback == exec_value:
            return True
        else:
            return False
