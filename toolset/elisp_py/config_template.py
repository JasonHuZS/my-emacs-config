#!/usr/bin/env python3

import sys
import ast
import utils
import json


def find_missed(toks, env):
    missed = []
    for tok in utils.find_concrete(toks):
        if tok[1].src not in env:
            missed.append(tok[1].src)
            env[tok[1].src] = utils.from_lisp_tok(tok[2][1])

    return missed


if __name__ == '__main__':
    with open(sys.argv[1]) as fd:
        config = fd.read()

    lisp_toks = ast.parse_lisp_inp(config)

    with open(sys.argv[2]) as fd:
        env = json.load(fd)

    missed = find_missed(lisp_toks, env)

    if len(missed):
        print('following fields are missing: {}'.format(
            ', '.join(missed)), file=sys.stderr)

    if len(sys.argv) >= 4:
        with open(sys.argv[3], 'w') as fd:
            json.dump(env, fd, indent=4, sort_keys=True)
    else:
        print(json.dumps(env, indent=4, sort_keys=True))

    sys.exit(0)
