import ast


def remove_src(t):
    if isinstance(t, ast.Literal):
        return t
    if isinstance(t, ast.PrefixedTree):
        if isinstance(t, ast.QuotedTree):
            return t.__class__(t.quote, remove_src(t.tree))
        return t.__class__(remove_src(t.tree))
    return ast.LispTree(map(remove_src, t), paren=t.paren)
