import ast
from literals import defconst, env_ident, repl_ident, nil, true
import string


def to_lisp_tok(v, skip_quote=False):
    if v is None:
        return nil
    if isinstance(v, (int, float)):
        return ast.Literal(str(v))
    if isinstance(v, bool):
        return true if v else nil
    if isinstance(v, str):
        return ast.Literal('"{}"'.format(v.replace('"', r'\"')))
    if isinstance(v, list):
        if not len(v):
            return nil
        lst = ast.LispTree([to_lisp_tok(vv, True) for vv in v],
                              '',
                              modified=True)
        if skip_quote:
            return lst
        return ast.QuotedTree("'", lst)
    raise Exception("unrecognized data of type {}: {}".format(type(v), v))


def from_lisp_tok(tok, quote=True):
    if quote and type(tok) == ast.QuotedTree and tok.quote == "'":
        if not len(tok):
            return []
        else:
            return [from_lisp_tok(t, False) for t in tok]

    if not quote and type(tok) == ast.LispTree:
        return [from_lisp_tok(t, False) for t in tok]
    elif not isinstance(tok, ast.Literal):
        raise Exception(
            'unsupported token type: {}, {}'.format(type(tok), tok))

    if tok == nil:
        return None
    if tok == true:
        return True
    if not len(tok.src):
        return ''

    if tok.src[0] not in string.digits + '"':
        raise Exception(
            'not sure how to translate this literal: {}'.format(tok))

    if tok.src[0] == '"' and tok.src[-1] == '"':
        return tok.src[1:-1].replace(r'\"', '"')
    if '.' in tok.src:
        return float(tok.src)
    return int(tok.src)


def find_templates(toks):
    for tok in toks:
        if not tok.prefixed \
           and tok.head() == defconst \
           and tok.paren == '()' \
           and len(tok) >= 3 \
           and tok[2].prefixed \
           and tok[2].tree == repl_ident:
            yield tok


def find_concrete(toks):
    for tok in toks:
        if not tok.prefixed \
           and tok.head() == defconst \
           and len(tok) >= 3 \
           and not tok[2].prefixed \
           and tok[2].head() == env_ident:
            yield tok
