#!/usr/bin/env python3

import ast
import sys
from literals import quote

toks = ast.parse_lisp_inp(sys.stdin.read())
custom_vars = ast.Literal("custom-set-variables")
sel_pkgs = ast.Literal("package-selected-packages")


def finish():
    print(ast.to_elisp(toks))
    sys.exit(0)


for tok in toks:
    if tok.head() == custom_vars:
        cusvar_tok = tok
        break
else:
    finish()

i = 0
while i < len(cusvar_tok):
    tok = cusvar_tok[i]
    if isinstance(tok, ast.QuotedTree) \
       and tok.head() == sel_pkgs \
       or tok.head() == quote \
       and tok[1][0] == sel_pkgs:
        del cusvar_tok[i]
        break
    i += 1

finish()
