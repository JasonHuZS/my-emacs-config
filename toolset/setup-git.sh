#!/bin/bash

set -x
tool_dir=`dirname $(readlink -f "${BASH_SOURCE[0]}")`

root=`git rev-parse --show-toplevel`

echo "Setting up git environment..."

cd "$root"

git config --local include.path ../.gitconfig

hooks=(post-commit)
for hook in "${hooks[@]}"; do
    ln -s -f "$(realpath --relative-to=.git/hooks/ "$tool_dir"/"$hook")" .git/hooks
done
