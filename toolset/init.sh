#!/bin/bash

set -x

function ddo() {
    code=$1
    shift
    "$@"
    exit $code
}

function die() {
    ddo "$1" echo "$2" >&2
}

function help() {
    if (($1 == 0)); then
        if [[ -n $2 ]]; then
            echo $2
        fi
    else
        echo $2 >&2
    fi
    ddo "$1" cat <<EOF
usage: $0 [-h]

-h    print help
EOF
    exit $1
}

function iif() {
    tf=$1
    shift
    if [[ $tf = "true" ]]; then
        "$@"
    fi
}

function unless() {
    tf=$1
    shift
    if [[ $tf = "false" ]]; then
        "$@"
    fi
}

dry=false

function run() {
    if [[ $dry = "true" ]]; then
        echo "dry running: $@"
    else
        echo "running: $@"
        "$@"
    fi
}

while getopts :hn opt; do
    case $opt in
        h) help 0;;
        n) dry=true;;
        ?) help 1 "-$OPTARG is not a valid option.";;
        :) help 1 "-$OPTARG is missing an argument.";;
    esac
done


tool_dir="`dirname $(readlink -f "${BASH_SOURCE[0]}")`"
cwd=`dirname "$tool_dir"`
install=(apt install -y)

cd "$cwd"

if [[ "$cwd" -ef ~/.emacs.d/ ]]; then
    in_emacsd=true
else
    in_emacsd=false
fi


run mkdir -p ~/.emacs.d
run unless $in_emacsd cp -r "$cwd/"* ~/.emacs.d

run iif $in_emacsd "$tool_dir"/setup-git.sh

run cp ~/.emacs.d/env.json.sample ~/.emacs.d/env.json
run echo 'you might want to configurate env.json manually.'
run ~/.emacs.d/toolset/gen-config.sh

# run mkdir -p ~/.emacs.d/pulls

# if hash coqc; then
#     cd ~/.emacs.d/pulls
#     git clone https://github.com/ProofGeneral/PG
    
#     if ! hash prooftree; then
#         git clone https://github.com/hendriktews/proof-tree
#     fi

#     (
#         cd PG;
#         make
#     )
# fi


if hash ocamlc; then
    if ! hash opam; then
        run sudo "${install[@]}" opam
    fi
fi
