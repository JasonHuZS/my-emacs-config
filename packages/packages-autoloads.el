;;; packages-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "alignments" "alignments.el" (0 0 0 0))
;;; Generated autoloads from alignments.el

(autoload 'align-region-sym-re-structured "alignments" "\
 align text from BEG to END (both inclusive). Very similar to align-range-sym-re-structured,
except that the align happens wihin the region instead of the line range.

\(fn BEG END SYM-RE &optional N DIR)" nil nil)

(autoload 'align-range-sym-re-structured "alignments" "\
 align text from LIN-BEG to LIN-END (both inclusive)
by matching N'th occurrence of SYM-RE.
 direction includes left alignment and right alignment.

\(fn LIN-BEG LIN-END SYM-RE &optional N DIR)" nil nil)

(autoload 'lookup-re-above "alignments" "\


\(fn RE STEP)" nil nil)

(autoload 'lookup-re-below "alignments" "\


\(fn RE STEP)" nil nil)

(autoload 're-between-spaces "alignments" "\


\(fn RE)" nil nil)

(autoload 'search-sym "alignments" "\


\(fn SYMS)" nil nil)

(autoload 'search-sym-region "alignments" "\


\(fn SYMS &optional RE-SPACE)" nil nil)

(autoload 'align-interactive-macro "alignments" "\


\(fn LANG DIR)" nil t)

(autoload 'align-region-interactive-macro "alignments" "\


\(fn LANG)" nil t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "alignments" '("align" "find-nth-occurrence" "left-align-coords" "lookup-re-by-direction" "match-begin-wtih-space" "right-align-coords" "trim-additional-spaces" "walk-back-spaces")))

;;;***

;;;### (autoloads nil "helper" "helper.el" (0 0 0 0))
;;; Generated autoloads from helper.el

(autoload 'gf/show-previous-sexp-macro-expand "helper" "\
Show the result of macro expanding the previous sexp.

\(fn)" t nil)

;;;***

;;;### (autoloads nil "hide-region" "hide-region.el" (0 0 0 0))
;;; Generated autoloads from hide-region.el

(autoload 'hide-region-hide "hide-region" "\
Hides a region by making an invisible overlay over it and save the
overlay on the hide-region-overlays \"ring\"

\(fn)" t nil)

(autoload 'hide-region-unhide "hide-region" "\
Unhide a region at a time, starting with the last one hidden and
deleting the overlay from the hide-region-overlays \"ring\".

\(fn)" t nil)

(autoload 'hide-region-unhide-all "hide-region" "\


\(fn INTERACTIVE)" nil nil)

(autoload 'hide-region-nest-hide "hide-region" "\
scan the file line by line, and hide the text in region between matching
START-TAG and END-TAG from file recursively, such that when unhiding, the text
appears in natural order.

\(fn START-TAG END-TAG &optional REG-BEG REG-END)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "hide-region" '("hide-region-")))

;;;***

;;;### (autoloads nil "redprl" "redprl.el" (0 0 0 0))
;;; Generated autoloads from redprl.el

(autoload 'redprl-mode "redprl" "\
Major mode for editing RedPRL proofs.
\\{redprl-mode-map}

\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.prl\\'" . redprl-mode))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "redprl" '("redprl-")))

;;;***

;;;### (autoloads nil "sr-speedbar" "sr-speedbar.el" (0 0 0 0))
;;; Generated autoloads from sr-speedbar.el

(autoload 'sr-speedbar-toggle "sr-speedbar" "\
Toggle sr-speedbar window.
Toggle visibility of sr-speedbar by resizing
the `sr-speedbar-window' to a minimal width
or the last width when visible.
Use this function to create or toggle visibility
of a speedbar-window.  It will be created if necessary.

\(fn)" t nil)

(autoload 'sr-speedbar-open "sr-speedbar" "\
Create `sr-speedbar' window.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "sr-speedbar" '("sr-speedbar-")))

;;;***

;;;### (autoloads nil "tutch-mode" "tutch-mode.el" (0 0 0 0))
;;; Generated autoloads from tutch-mode.el

(autoload 'tutch-mode "tutch-mode" "\
Major mode for editing tuch files" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "tutch-mode" '("tutch-")))

;;;***

;;;### (autoloads nil "typer-mode" "typer-mode.el" (0 0 0 0))
;;; Generated autoloads from typer-mode.el

(add-to-list 'auto-mode-alist '("\\.typer\\'" . typer-mode))

(add-to-list 'auto-coding-alist '("\\.typer\\'" . utf-8))

(autoload 'typer-mode "typer-mode" "\
A major mode for editing Typer files.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "typer-mode" '("typer-")))

;;;***

(provide 'packages-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; packages-autoloads.el ends here
