(use-package dash)

(defun match-begin-wtih-space (s)
  (and (not (seq-empty-p s)) (equal (elt s 0) ?\s)))

(defun align-match-struct (lin col len)
  "the data structure for storing match results."
  `(:lin ,lin :col ,col :len ,len))

(defun walk-back-spaces ()
  (let ((res nil))
    (while (and (> (point) (line-beginning-position))
                (equal (char-after) ?\s))
      (setq res (1- (point)))
      (goto-char res))
    res))

(defun trim-additional-spaces ()
  "trim the spaces from current point backwards until one space is left."
  (if (equal (char-after) ?\s)
      (let* ((cur-pos (point))
             (new-pos (save-excursion
                        (walk-back-spaces)
                        (unless (equal (char-after) ?\s)
                          (goto-char (1+ (point))))
                        (point))))
        (if new-pos
            (if (equal new-pos (line-beginning-position))
                (goto-char (1+ (point)))
              (delete-region new-pos (1+ cur-pos))
              (insert ?\s))))))

(defun align-goto-coord (coord)
  (goto-coord (plist-get coord :lin) (plist-get coord :col)))

(defun left-align-coords (coords)
  "based on `match-struct'"
    (unless (seq-empty-p coords)
      (let ((max-col (apply 'max (--map (plist-get it :col) coords))))
        (--each coords
          (save-excursion
            (align-goto-coord it)
            (insert (make-string (- max-col (plist-get it :col)) ?\s)))))))

(defun right-align-coords (coords)
  (unless (seq-empty-p coords)
    (let ((max-col (apply 'max (--map (+ (plist-get it :col)
                                         (plist-get it :len))
                                      coords))))
      (--each coords
        (save-excursion
          (align-goto-coord it)
          (insert (make-string (- max-col (+ (plist-get it :col)
                                             (plist-get it :len)))
                               ?\s)))))))

(defconst alignment-methods
  '(left left-align-coords
    right right-align-coords))

(defun find-nth-occurrence (re n &optional search-end)
  "find the N'th occurrence of RE of the current line."
  (setq search-end (or search-end (line-end-position)))
  
  (save-excursion
    (let ((failed nil))
      (while (and (not failed) (> n 0))
        (let ((end-pos (re-search-forward re
                                          (line-end-position) t 1)))
          (if end-pos
              (setq n (1- n))
            (setq failed t))))
      (if failed
          nil
        (re-search-backward re
                            (line-beginning-position)
                            t 1)))))


(defun align--compute-new-end (lin col)
  (save-excursion
    (goto-coord lin col)
    (point)))

;;;###autoload
(defun align-region-sym-re-structured (beg end sym-re &optional n dir)
  " align text from BEG to END (both inclusive). Very similar to align-range-sym-re-structured,
except that the align happens wihin the region instead of the line range."

  (setq n (or n 1))
  (setq dir (or dir 'left))
  (setq beg (if (< beg (point-min)) (point-min) beg))
  (setq end (cond
             ((< end (point-min)) (point-min))
             ((> end (point-max)) (point-max))
             (t end)))

  (let* ((end-line (line-number-at-pos end))
         (end-col (col-at-pos end))
         (dir-func (plist-get alignment-methods dir))
         (temp-list '())
         (syms
          (save-excursion
            (goto-char beg)
            (while (< (point) (align--compute-new-end end-line end-col))
              (let* ((lin-end (line-end-position))
                     (search-end (if (< lin-end end) lin-end end))
                     (start-pos (find-nth-occurrence sym-re n search-end)))
                (if start-pos
                    (progn
                      (goto-char start-pos)
                      (trim-additional-spaces)
                      (push (align-match-struct (line-number-at-pos)
                                                (col-at-pos)
                                                (length (match-string-no-properties 1)))
                            temp-list)))
                (forward-line)))
            (reverse temp-list))))
    (funcall dir-func syms)))


;;;###autoload
(defun align-range-sym-re-structured (lin-beg lin-end sym-re &optional n dir)
  " align text from LIN-BEG to LIN-END (both inclusive)
by matching N'th occurrence of SYM-RE.
 direction includes left alignment and right alignment."
  
  (setq n (or n 1))
  (setq dir (or dir 'left))
  (setq lin-beg (if (< lin-beg 1) 1 lin-beg))
  (setq lin-end (let ((lin-max (line-number-at-pos (point-max))))
                  (if (> lin-end lin-max) lin-max lin-end)))

  (align-region-sym-re-structured (line-beginning-position-abs lin-beg)
                                  (line-end-position-abs lin-end)
                                  sym-re
                                  n
                                  dir))


(defun lookup-re-by-direction (re step dir)
  " look up RE by provided direction DIR. positive goes down and negative goes up, every
iteration goes up or down by (abs DIR). 

RE will be attempted to match for each line,if it's matched, then it looks one line above;
or it decreases STEP by 1. function stops either file margin is hit, or STEP goes to 0."

  (if (or (= dir 0) (< step 0))
      (line-number-at-pos (point))

    (save-excursion
      (forward-line 0)
      (unless (re-search-forward re (line-end-position) t 1)
        (setq step (1- step)))

      (while (and (> step 0)
                  (> (line-number-at-pos (point)) (line-number-at-pos (point-min)))
                  (< (line-number-at-pos (point)) (line-number-at-pos (point-max))))
        (forward-line dir)
        (unless (re-search-forward re (line-end-position) t 1)
            (setq step (1- step))))
      
      (line-number-at-pos (point)))))

;;;###autoload
(defun lookup-re-above (re step)
  (lookup-re-by-direction re step -1))

;;;###autoload
(defun lookup-re-below (re step)
  (lookup-re-by-direction re step 1))


;;;###autoload
(defun re-between-spaces (re)
  (concat "[[:space:]]"
          "\\("
          re
          "\\)"
          "\\(?:[[:space:]]\\|(\\|)\\|$\\)"))

;;;###autoload
(defun search-sym (syms)
  (let ((res (catch 'loop-break
               (--each syms
                 (save-excursion
                   (if (re-search-forward it (line-end-position) t 1)
                       (throw 'loop-break it)))))))
    (if res res
      (or (catch 'loop-break
            (--each syms
              (save-excursion
                (if (re-search-backward it (line-beginning-position) t 1)
                    (throw 'loop-break it)))))
          nil))))


;;;###autoload
(defun search-sym-region (syms &optional re-space)
  (let* ((dir (if (= (point) (region-beginning)) 'right 'left))
         (opposite (if (equal dir 'left) 'right 'left))
         (quote-func (if re-space 're-between-spaces 'identity))
         (right (lambda ()
                  (catch 'loop-break
                    (--each (number-sequence 0 (1- (length syms)))
                      (save-excursion
                        (if (re-search-forward (funcall quote-func (elt syms it)) (region-end) t 1)
                            (throw 'loop-break it)))))))
         (left (lambda ()
                 (catch 'loop-break
                   (--each (number-sequence 0 (1- (length syms)))
                     (save-excursion
                       (if (re-search-backward (funcall quote-func (elt syms it)) (region-beginning) t 1)
                           (throw 'loop-break it)))))))
         (res (funcall (symbol-value dir))))
    (if res res
      (or (funcall (symbol-value opposite)) nil))))

;;;###autoload
(defmacro align-interactive-macro (lang dir)
  (let* ((l (cadr lang))
         (d (cadr dir))
         (fname (intern (format "%s-align-sym-re-by-region-%s" l d)))
         (align-symbols (intern (format "%s-align-symbols" l)))
         (align-symbols-history (intern (format "%s-align-symbols-history" l)))
         (search-sym (intern (format "%s-search-sym" l))))
    `(defun ,fname
         (re lin-beg lin-end n)
       "alignment function to prettify the code.
control behavior by prefix key C-u:

none    align the first symbol that's able to recognize,
once    ask for the symbol it should align.
twice   ask for both the symbol and which occurrence it should align.
thrice  ask for the symbol only and align all occurrence of the symbol.
4x      same as thrice, except do not call 're-between-spaces. "
       (interactive
        (let ((sym (if current-prefix-arg
                       (completing-read "symbols to align: "
                                        ,align-symbols
                                        nil 'confirm nil
                                        ',align-symbols-history
                                        (,search-sym))
                     (,search-sym)))
              (nth (if (equal (prefix-pressed) 2)
                       (string-to-number
                        (completing-read "which occurrence?: "
                                         (--map (number-to-string it) (number-sequence 1 10))
                                         nil nil nil nil
                                         "1"))
                     1)))
          (if (region-active-p)
              (list sym
                    (line-number-at-pos (region-beginning))
                    (line-number-at-pos (region-end))
                    nth)
            (list sym
                  (lookup-re-above "[^[:space:]]" 1)
                  (lookup-re-below "[^[:space:]]" 1)
                  nth))))

       (if (not re)
           (error "cannot find any alignment symbols!")
         (pcase (prefix-pressed)
           (`3 (dolist (n1 (number-sequence 1 10)) ; if C-u thrice, we align as much as possible
                 (align-range-sym-re-structured lin-beg
                                                lin-end
                                                (re-between-spaces re)
                                                n1
                                                ,dir)))
           (`4 (dolist (n1 (number-sequence 1 10))
                 (align-range-sym-re-structured lin-beg lin-end (concat "\\(" re "\\)") n1 ,dir)))
           (_ (align-range-sym-re-structured lin-beg lin-end (re-between-spaces re) n ,dir)))
         (message (format "aligned according to %s with %s prefix(es)." re (prefix-pressed)))))))

;;;###autoload
(defmacro align-region-interactive-macro (lang)
  (let* ((l (cadr lang))
         (fname (intern (format "%s-align-region-interactive" l)))
         (align-symbols (intern (format "%s-align-symbols" l)))
         (align-region-symbols-history (intern (format "%s-align-region-symbols-history" l)))
         (search-sym-region (intern (format "%s-search-sym-region" l))))

    `(defun ,fname (re beg end n dir)
      "alignment function to prettify the code in region.
control behavior by prefix key C-u:

none    align the first symbol that's able to recognize,
once    ask for the symbol it should align.
twice   ask for both the symbol and which occurrence it should align.
thrice  ask for the symbol and alignment direction and align all occurrence of the symbol.
4x      same as thrice, except do not call 're-between-spaces. "

      (interactive
       (if (not (region-active-p))
           (error "region is not active!")
         (let* ((symbols (append ,align-symbols
                                 ,align-region-symbols-history))
                (sym-idx (,search-sym-region symbols (= (prefix-pressed) 4)))
                (sym (if current-prefix-arg
                         (completing-read "symbols to align: " ,align-symbols
                                          nil 'confirm nil
                                          ',align-region-symbols-history
                                          (elt symbols sym-idx))
                       (elt symbols sym-idx)))
                (nth (if (= (prefix-pressed) 2)
                         (string-to-number
                          (completing-read "which occurrence?: "
                                           (--map (number-to-string it) (number-sequence 1 10))
                                           nil nil nil nil
                                           "1"))
                       1))
                (dir (if (>= (prefix-pressed) 2)
                         (intern
                          (completing-read "which alignment?: "
                                           '(left right)
                                           nil nil nil nil
                                           "right"))
                       'right)))
           (list sym (region-beginning) (region-end) nth dir))))

      (if (not re)
          (error "cannot find any alignment symbols!")
        (pcase (prefix-pressed)
          (`3 (dolist (n1 (number-sequence 1 10))
                (align-region-sym-re-structured beg end (re-between-spaces re) n1 dir)))
          (`4 (dolist (n1 (number-sequence 1 10))
                (align-region-sym-re-structured beg end (concat "\\(" re "\\)") n1 dir)))
          (_ (align-region-sym-re-structured beg end (re-between-spaces re) n dir)))
        (message (format "aligned to %s according to %s with %s prefix(es)." dir re (prefix-pressed)))))))

(provide 'alignments)

;;; alignments.el ends here
