(global-hi-lock-mode 1)
(set-face-attribute 'default nil :height 130) ; set default face hight be 13px
(setq inhibit-startup-message t)        ; hide the startup message
(setq isearch-allow-scroll t)
(setq global-mark-ring-max 128          ; enlarge global mark ring
      mark-ring-max 128)
(setq make-backup-files nil)            ; turn off backup files
(setq set-mark-command-repeat-pop t)    ; allows continuous presses of C-SPC
(setq enable-recursive-minibuffers t)
(if (version< emacs-version "25")
    (progn
      (require 'saveplace)
      (setq-default save-place t))      ; emacs remember file position
  (save-place-mode t))
(setq-default eldoc-idle-delay 0)       ; set eldoc delay to be 0

(setq-default indent-tabs-mode nil)

(setq-default fill-column 86)

;; CUTOMIZED KEY BINDINGS
;; prefix key C-x c
;; easier to open the config file
(bind-keys
 ("C-c config" . (lambda () (interactive) (find-file (from-emacsd "init.el"))))
 ("C-x c k"    . kill-whole-line)
 ("C-x j"      . rev-join-line)
 ("C-c w"      . mark-current-word)
 ("C-c i"      . mark-line)
 ("M-g shell"  . launch-shell)
 ("M-g C-SPC"  . unpop-to-mark-command))

(put 'narrow-to-region 'disabled nil)
(put 'LaTeX-narrow-to-environment 'disabled nil)
(put 'company-coq-fold 'disabled nil)
(put 'TeX-narrow-to-group 'disabled nil)

(global-undo-tree-mode)

(let ((default-font "-GNU -FreeMono-normal-normal-normal-*-15-*-*-*-m-0-iso10646-1"))
  (add-to-list 'default-frame-alist `(font . ,default-font)))
