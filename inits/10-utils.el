(use-package magit)
(use-package dash)

(defun to-symbol-definition ()
  "jump to the definition of current symbol"
  (interactive)
  (imenu (current-word)))

(defun developer-bundle ()
  "it defines a set of setups that might be helpful\
 for general development purpose"
  (display-line-numbers-mode t)
  (smartparens-mode t)
  (company-mode t)
  (origami-mode t)
  (rainbow-delimiters-mode t)
  (projectile-mode t)
  (yas-minor-mode t)
  (flycheck-mode t)
  (undo-tree-mode t)
  (fic-mode t)
  (git-gutter-mode t)

  (bind-keys :map (current-local-map)
             ("C-x p"   . company-complete)
             ;; ("C-x M-m" . imenu)
             ;; ("C-x m"   . to-symbol-definition)
             ;; ("C-x M-c" . (lambda ()
             ;;                "rescan the cache for imenu."
             ;;                (interactive) (imenu "*Rescan*")))
             ("C-c '"   . avy-flycheck-goto-error)
             ;; ("C-c f"   . projectile-find-file)
             ;; ("C-c F"   . projectile-grep)
             ("C-c g"   . magit-status)))


(defun rev-join-line (&optional arg)
  "do the same as 'join-line but ARG means the opposite."
  (interactive "*P")
  (join-line (not arg)))

(defun to-int-or-default (arg default)
  "convert a prefix command to an integer or DEFAULT if it's nil."
  (if arg (prefix-numeric-value arg) default))

(defun launch-shell ()
  "launch shell inside of emacs."
  (interactive)
  (ansi-term shell-location))

(defun select-region-if-active (&optional defl)
  "crop the substring if a region is active. otherwise DEFL will be called
as a function to retreave default value."
  (if (use-region-p)
      (progn
        (deactivate-mark)
        (buffer-substring-no-properties
         (region-beginning) (region-end)))
    (and defl (apply defl ()))))

(defun unpop-to-mark-command ()
  "Unpop off mark ring. Does nothing if mark ring is empty."
  (interactive)
  (when mark-ring
    (setq mark-ring (cons (copy-marker (mark-marker)) mark-ring))
    (set-marker (mark-marker) (car (last mark-ring)) (current-buffer))
    (when (null (mark t)) (ding))
    (setq mark-ring (nbutlast mark-ring))
    (goto-char (marker-position (car (last mark-ring))))))

;; CUSTOMIZED MARKING

(defun find-current-word (&optional strict)
  "find the position of current word. set STRICT to amek it strictly a word.
returns nil if no word around, or a tuple of (begin . end)"
  )

(defun mark-current-word (&optional strict)
  "mark the current symbol, set STRICT to make it strictly a word."
  (interactive "P")
  (let (start
	tfwd
	(current (point))
	(syntaxes (if strict "w" "w_")))
    (save-excursion
      (setq tfwd (skip-syntax-backward syntaxes))
      (setq start (point)))
    (push-mark start nil t)
    (- (skip-syntax-forward syntaxes) tfwd)))


(defun end-of-n-line (n)
  "move to the end of n'th line"
  (interactive "P")
  (setq n (to-int-or-default n 1))
  (next-line n t)
  (move-end-of-line 1))

(defun begin-of-n-line (n)
  "move to the beginning of n'th line"
  (interactive "P")
  (setq n (to-int-or-default n 1))
  (previous-line n t)
  (move-beginning-of-line 1))


(defun mark-current-line ()
  "mark the current line."
  (interactive)
  (move-beginning-of-line 1)
  (push-mark (point) nil t)
  (move-end-of-line 1))


(defun mark-line (&optional num)
  "mark NUM number of lines staring from the current line."
  (interactive "P")
  (setq num (to-int-or-default num 1))
  (cond
   ((or (eq last-command 'mark-line) (region-active-p))
    (if (<= (mark) (point))
	(end-of-n-line num)
      (begin-of-n-line num)))
   (t (when (not (= num 0))
	(mark-current-line)
	(end-of-n-line (1- num))))))

(defun goto-line-num (n)
  (goto-char (point-min))
  (forward-line (1- n)))

(defun line-beginning-position-abs (n)
  (save-excursion
    (goto-char (point-min))
    (line-beginning-position n)))

(defun line-end-position-abs (n)
  (save-excursion
    (goto-char (point-min))
    (line-end-position n)))

(defun goto-coord (lin col)
  (goto-line-num lin)
  (move-to-column col))

(defun safe-set-face (f face &rest args)
  (if (facep face)
      (apply f (cons face args))))

(defun col-at-pos (&optional pos)
  (setq pos (or pos (point)))
  (save-excursion
    (goto-char pos)
    (current-column)))

(defun prefix-pressed (&optional n)
  (let ((count
         (if current-prefix-arg
             (round (log (car current-prefix-arg) 4))
           0)))
    (or (and n (equal n count))
        count)))

(defun set-frame-font-height (size)
  (interactive "nSet frame font height: \n")
  (set-face-attribute 'default (selected-frame) :height size))
