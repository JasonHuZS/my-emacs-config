(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq package-enable-at-startup nil)

(straight-use-package 'use-package)

;; set up bindings to allow easier move among windows
(use-package windmove
  :bind (("C-c h" . windmove-left)
         ("C-c j" . windmove-down)
         ("C-c k" . windmove-up)
         ("C-c l" . windmove-right)))

(use-package hippie-exp
  :bind (("M-/" . hippie-expand)))

(use-package hideshow
  :diminish hs-minor-mode)

(use-package outline
  :diminish outline-minor-mode)

(use-package warnings
  :config
  (add-to-list 'warning-suppress-types '(undo discard-info)))

(use-package diminish
  :ensure t)

(use-package material-theme
  :ensure t
  :config
  (if (daemonp)
      (add-hook 'after-make-frame-functions (lambda (frame)
                        (when (eq (length (frame-list)) 2)
                          (progn
                            (select-frame frame)
                            (load-theme 'material)
                            (let ((default-font
                                    "-GNU -FreeMono-normal-normal-normal-*-15-*-*-*-m-0-iso10646-1"))
                              (set-frame-font default-font))
))))
    (load-theme 'material)))     ; load material theme

(use-package powerline
  :ensure t
  :config
  (powerline-default-theme))

;; (use-package helm-swoop
;;   :ensure t
;;   :bind (("C-c o" . helm-swoop)
;;          ("C-c s" . helm-multi-swoop-all)
;;          :map isearch-mode-map
;;          ;; When doing isearch, hand the word over to helm-swoop
;;          ("M-i" . helm-swoop-from-isearch)
;;          :map helm-swoop-map
;;          ;; From helm-swoop to helm-multi-swoop-all
;;          ("M-i" . helm-multi-swoop-all-from-helm-swoop))
;;   :config
;;   ;; Save buffer when helm-multi-swoop-edit complete
;;   (setq helm-multi-swoop-edit-save t)
;;   ;; If this value is t, split window inside the current window
;;   (setq helm-swoop-split-with-multiple-windows t)
;;   ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
;;   (setq helm-swoop-split-direction 'split-window-vertically)
;;   ;; If nil, you can slightly boost invoke speed in exchange for text color
;;   (setq helm-swoop-speed-or-color t)
;;   ;; prevent swoop taking symbol at point
;;   (setq helm-swoop-pre-input-function (lambda () nil)))

;; avy setup
(use-package avy
  :ensure t
  :bind (("M-g :" . avy-goto-char)
         ("M-g '" . avy-goto-char-2)
         ("M-g g" . avy-goto-line)
         ("M-g w" . avy-goto-word-1)))

(use-package avy-flycheck
  :ensure t
  :config
  (avy-flycheck-setup)
  :bind (("C-c '" . avy-flycheck-goto-error)))

(use-package swap-buffers
  :ensure t
  :bind (("C-c b" . swap-buffers)))

;; window switching
(use-package switch-window
  :ensure t
  ;; :config
  ;; (setq switch-window-shortcut-appearance 'asciiart)
  :bind (("C-x o" . switch-window)
         ("C-x 1" . switch-window-then-maximize)   
         ("C-x 2" . switch-window-then-split-below)
         ("C-x 3" . switch-window-then-split-right)
         ("C-x 0" . switch-window-then-delete)))

(use-package free-keys
  :ensure t
  :bind (("C-h C-k" . free-keys)))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :init
  ;; smartparens settings
  (use-package smartparens-config)
  (show-smartparens-global-mode)
  (setq show-paren-delay 0)
  :bind (:map smartparens-mode-map
              ("C-M-f"   . sp-forward-sexp)
              ("C-M-b"   . sp-backward-sexp)
              ("C-M-SPC" . sp-mark-sexp)))

;; fiplr settings
(use-package fiplr
  :ensure t
  :init
  (setq fiplr-root-markers '(".git" ".svn"))
  (setq fiplr-ignored-globs '((directories (".git" ".svn"))
                              (files ("*.jpg" "*.png" "*.zip" "*~")))))

(use-package popup-imenu
  :ensure t
  :commands popup-imenu
  :bind ("M-i" . popup-imenu))

(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :config
  (setq undo-tree-visualizer-diff t
	undo-tree-visualizer-timestamps t)
  (setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
  (global-undo-tree-mode))

(use-package goto-chg
  :ensure t
  ;; complementary to
  ;; C-x r m / C-x r l
  ;; and C-<space> C-<space> / C-u C-<space>
  :bind (("C-c ." . goto-last-change)
         ("C-c ," . goto-last-change-reverse)))

(use-package origami
  :ensure t
  :bind (:map origami-mode-map
              ("M-g A" . origami-recursively-toggle-node)
              ("M-g O" . origami-open-node-recursively)
              ("M-g o" . origami-open-node)
              ("M-g C" . origami-close-node-recursively)
              ("M-g c" . origami-close-node)
              ("M-g R" . origami-open-all-nodes)
              ("M-g M" . origami-close-all-nodes)))

(use-package s
  :ensure t)

(use-package dash
  :ensure t)

(use-package flycheck
  :ensure t
  :diminish flycheck-mode)

(use-package company
  :ensure t)

(use-package company-flx
  :ensure t
  :config
  (with-eval-after-load 'company
    (company-flx-mode 1)))

(use-package yasnippet
  :ensure t)

(use-package yasnippet-snippets
  :ensure t)

(use-package expand-region
  :ensure t)

(use-package magit
  :ensure t
  :diminish auto-revert-mode
  :bind (:map magit-status-mode-map
              (","     . magit-section-toggle)
              ("C-,"   . magit-section-cycle)
              ("M-,"   . magit-section-cycle-diffs)
              ("C-M-," . magit-section-cycle-global)))

(use-package magit-popup
  :ensure t)

(use-package rainbow-delimiters
  :ensure t)

(use-package projectile
  :ensure t
  :init
  (setq projectile-use-git-grep t)
  (setq projectile-mode-line
        '(:eval (format " P[%s]" (projectile-project-name)))))

(use-package mmm-mode
  :ensure t)

(use-package dtrt-indent
  :ensure t
  :init (dtrt-indent-mode))

(use-package ws-butler
  :ensure t
  :init
  (add-hook 'text-mode 'ws-butler-mode)
  (add-hook 'fundamental-mode 'ws-butler-mode))

(use-package volatile-highlights
  :ensure t
  :diminish volatile-highlights-mode
  :init
  (volatile-highlights-mode t))

(use-package anzu
  :ensure t
  :diminish anzu-mode
  :bind (("M-%" . anzu-query-replace)
         ("C-M-%" . anzu-query-replace-regexp))
  :init
  (global-anzu-mode))

(use-package iedit
  :ensure t
  :bind (("C-;" . iedit-mode))
  :init
  (setq iedit-toggle-key-default nil))

;; ido mode setup
(use-package ido
  :config
  ;; (ido-mode 1)
  ;; show choices vertically
  (if (version< emacs-version "25")
      (setq ido-separator "\n")
    (progn
      (make-local-variable 'ido-decorations)
      (setf (nth 2 ido-decorations) "\n")))
  
  ;; show any name that has the chars you typed
  (setq ido-enable-flex-matching t)
  ;; use current pane for newly opened file
  (setq ido-default-file-method 'selected-window)
  ;; use current pane for newly switched buffer
  (setq ido-default-buffer-method 'selected-window))

(use-package fic-mode
  :ensure t)

(use-package git-gutter-fringe
  :ensure t
  :diminish git-gutter-mode)

(use-package lsp-mode
  :ensure t
  :commands lsp
  :config
  (define-key lsp-mode-map (kbd "C-c l") lsp-command-map)
  (add-to-list 'company-backends 'company-capf))

(use-package lsp-ui
  :ensure t)

(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-directory-name-transformer    #'identity
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              5000
          treemacs-file-extension-regex          treemacs-last-period-regex-value
          treemacs-file-follow-delay             0.2
          treemacs-file-name-transformer         #'identity
          treemacs-follow-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-move-forward-on-expand        nil
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                      'left
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-asc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-user-mode-line-format         nil
          treemacs-user-header-line-format       nil
          treemacs-width                         35)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after treemacs magit
  :ensure t)

(use-package lsp-treemacs
  :ensure t)

(use-package ws-butler
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'ws-butler-mode))
