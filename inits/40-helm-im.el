;; display input method candidates using helm

(use-package dash)
(use-package helm)
(use-package helm-lib)
(use-package helm-utils)


(defun helm-im-build-candidates ()
  "turn quail-map into a alist for map so that helm can use it"
  (defun unpack-char (c)
    (pcase c
      ((pred integerp) c)
      ((pred stringp) (elt c 0))
      (`(,cc) (unpack-char cc))
      (`[,cc] (unpack-char cc))
      (`(,(pred listp) .
         ,(and tail (pred vectorp)))
       (unpack-char tail))
      ((pred sequencep)
       (--map (unpack-char it) c))
      (cc cc)))
  
  (defun dfs (tree prefix-acc lacc)
    (pcase tree
      (`nil lacc)
      (`(,c ,r . ,children)
       (let* ((nprefix (cons c prefix-acc))
              (nacc (if r
                        (cons
                         `(,(concat (reverse nprefix)) . ,(unpack-char r))
                         lacc)
                      lacc)))
         (--reduce-from
          (dfs it nprefix acc)
          nacc
          children)))))

  (defun extract-quail-map ()
    (pcase quail-package-alist
      (`((,_ ,_ (,_ . ,map) . ,_) . ,_) map)))
  
  (let* ((quail-tree (extract-quail-map)))
    (--reduce-from
     (dfs it nil acc)
     nil
     quail-tree)))


(defvar-local helm-im-serialized-symbols
  nil
  "")

(defvar-local helm-im-init-im
  nil
  "")

(defvar-local helm-im-history
  nil
  "")

(defun helm-im-init ()
  (if current-input-method
      (progn
        (setq-local helm-im-init-im
                    current-input-method)
        (setq-local helm-im-serialized-symbols
                    (helm-im-build-candidates))
        t)
    nil))


(defun helm-im-initialied? ()
  (and helm-im-init-im (equal helm-im-init-im current-input-method)))


(defun helm-im-init-by-need ()
  (if (not (helm-im-initialied?))
      (if (helm-im-init)
          t
        (error "helm-im failed to initiailze symbols.")
        nil)
    t))

(defvar helm-source-im
  (helm-build-sync-source "helm-im"
    :candidates (lambda () helm-im-serialized-symbols)
    :action (lambda (s) (message "%s" (string s)))
    :history 'helm-im-history))

(defun helm-im ()
  (interactive)
  (if (helm-im-init-by-need)
      (helm :sources (helm-build-sync-source "helm-im"
    :candidates helm-im-serialized-symbols
    :action (lambda (s) (message "%s" (string s)))
    :history 'helm-im-history)
            :buffer "Helm Im Test"
            :allow-nest t
            :history 'helm-im-history)))
