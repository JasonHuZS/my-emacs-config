;; set up executable paths
(let ((add-exec-path (lambda (path)
                       (let ((p (expand-file-name path)))
                         (setenv "PATH" (concat p path-separator (getenv "PATH")))
                         (add-to-list 'exec-path p)))))
  (dolist (path additional-exec-path)
    (funcall add-exec-path path)))
