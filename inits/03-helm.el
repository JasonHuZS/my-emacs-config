(use-package helm
  :ensure t
  :diminish helm-mode
  :bind (("M-x"       . helm-M-x)
         ("M-y"       . helm-show-kill-ring)
         ("M-\""      . helm-eval-expression)
         ("C-x b"     . helm-mini)
         ("C-x C-f"   . helm-find-files)
         ("C-x f"     . helm-locate)
         ("C-h SPC"   . helm-mark-ring)
         ("C-h C-SPC" . helm-global-mark-ring)
         ("C-h a"     . helm-apropos)
         ("C-c o"     . helm-occur))
  :config
  (setq helm-scroll-amount 8
        helm-autoresize-max-height 40
        helm-autoresize-min-height 20
        helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match t
        helm-semantic-fuzzy-match t
        helm-imenu-fuzzy-match t
        helm-locate-fuzzy-match t)
  (helm-autoresize-mode 1)
  (helm-mode 1))

(use-package helm-projectile
  :ensure t
  :bind (("C-x c C-p" . helm-projectile-grep))
  :init
  (helm-projectile-on)
  (setq projectile-completion-system 'helm)
  (setq projectile-indexing-method 'alien))


(defvar my-helm-follow-sources '()
  "List of sources for which helm-follow-mode should be enabled")

;; Use helm-follow-mode for the following sources:
(add-to-list 'my-helm-follow-sources 'helm-source-occur)
(add-to-list 'my-helm-follow-sources 'helm-source-moccur)
(add-to-list 'my-helm-follow-sources 'helm-source-grep-ag)
(add-to-list 'my-helm-follow-sources 'helm-source-grep)
(add-to-list 'my-helm-follow-sources 'helm-source-global-mark-ring)
(add-to-list 'my-helm-follow-sources 'helm-source-mark-ring)

(defun my-helm-set-follow ()
  "Enable helm-follow-mode for the sources specified in the list
variable `my-helm-follow-sources'. This function is meant to
be run during `helm-initialize' and should be added to the hook
`helm-before-initialize-hook'."
  (mapc (lambda (source)
          (when (memq source my-helm-follow-sources)
            (helm-attrset 'follow 1 (symbol-value source))))
        helm-sources))

;; Add hook to enable helm-follow mode for specified helm 
(add-hook 'helm-before-initialize-hook 'my-helm-set-follow)
