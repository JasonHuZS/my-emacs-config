(defun tutch-mode-setup ()
  (interactive)
  (developer-bundle))

(add-hook 'tutch-mode-hook 'tutch-mode-setup)
(add-to-list 'auto-mode-alist '("\\.tut\\'" . tutch-mode))
