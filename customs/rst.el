(defun rst-mode-setup ()
  (interactive)
  (developer-bundle)
  (auto-fill-mode))

(with-eval-after-load 'rst
  (add-to-list 'rst-mode-hook 'rst-mode-setup))


