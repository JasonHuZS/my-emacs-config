(defun makefile-setup ()
  (interactive))

(add-hook 'makefile-mode-hook 'makefile-setup)
(add-hook 'makefile-gmake-mode-hook 'makefile-setup)
