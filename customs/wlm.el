;;;###autoload
(define-derived-mode wlm-mode c-mode "WLM"
  "Major mode for WLM language at University of Waterloo.")


(add-to-list 'auto-mode-alist '("\\.wlm\\'" . wlm-mode))
