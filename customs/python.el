;; python related
(use-package elpy
  :ensure t
  :hook (python-mode . lsp)
  :config
  (elpy-enable)
  (setq eldoc-idle-delay 0.1)
  (setq python-shell-interpreter "ipython")
  (setq python-shell-interpreter-args "-i --simple-prompt")
  (when (load "flycheck" t t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'flycheck-mode))
  :bind (:map python-mode-map
              ("M-."   . lsp-ui-peek-find-definitions)
              ("C-c r" . lsp-ui-peek-find-references)))

(use-package py-autopep8
  :ensure t
  :config
  (add-hook 'python-mode-hook 'py-autopep8-enable-on-save))

(use-package python-environment
  :ensure t)

(use-package pyvenv
  :ensure t)

(with-eval-after-load "elpy-rpc"
  (defun elpy-rpc--create-virtualenv (rpc-venv-path)
    "Create a virtualenv for the RPC in RPC-VENV-PATH."
    ;; venv cannot create a proper virtualenv from inside another virtualenv
    (let ((elpy-rpc-virtualenv-path 'global)
          (the-command elpy-rpc-python-command))
      (with-elpy-rpc-virtualenv-activated
       (cond
        ((= 0 (call-process elpy-rpc-python-command nil nil nil
                            "-m" "venv" "-h"))
         (with-current-buffer (generate-new-buffer "*venv*")
           (call-process the-command nil t t
                         "-m" "venv" rpc-venv-path)))
        ((executable-find "virtualenv")
         (with-current-buffer (generate-new-buffer "*virtualenv*")
           (call-process "virtualenv" nil t t
                         "-p" elpy-rpc-python-command rpc-venv-path)))
        (t
         (error "Elpy necessitates the 'virtualenv' python package, please install it with `pip install virtualenv`"))))))
  )

;; (defun python-setup ()
;;   (interactive)
;;   (let ((root (projectile-project-root)))
;;     (if root
;;         (if (file-exists-p (concat root ".use-python3"))
;;             (progn
;;               (setq-local elpy-rpc-python-command "python3")
;;               (setq-local python-shell-interpreter "ipython3"))
;;           ))))

;; (add-hook 'elpy-mode-hook 'python-setup)

(defun python-inject-multiline (func arg-string)
  "A code template to inject multiline text in python"
  (let* ((indentation (save-excursion
                                    (goto-char start-point)
                                    (current-indentation)))
         (indent-func (lambda (&optional additional-depth)
                        (setq additional-depth (or additional-depth 0))
                        (make-string (+ indentation (* additional-depth python-indent-offset))
                                     ?\s))))
    (mapconcat (lambda (arg) (funcall func arg indent-func))
               (elpy-snippet-split-args arg-string)
               "")))


(defun python-snippet-init-assignments (arg-string)
  "Return the typical __init__ assignments for arguments."
  (python-inject-multiline
   (lambda (arg indent-func)
     (if (string-match "^\\*" (car arg))
         ""
       (format "self._%s = %s\n%s"
               (car arg)
               (car arg)
               (funcall indent-func))))
   arg-string))

(defun python-snippet-getters (arg-string)
  "Return the boiler plates for properties."
  (python-inject-multiline
   (lambda (arg indent-func)
     (if (string-match "^\\*" (car arg))
         ""
       (concat
        "@property\n"
        (funcall indent-func) (format "def %s(self):\n" (car arg))
        (funcall indent-func 1)
        (format "return self._%s\n\n"
                (car arg))
        (funcall indent-func))))
   arg-string))

(defun python-snippet-params (arg-string)
  "return the string for :param: for function arguments."
  (python-inject-multiline
   (lambda (arg indent-func)
     (if (string-match "^\\*" (car arg))
         ""
       (concat ":param " (car arg) ": \n" (funcall indent-func))))
   arg-string))

(defun python-repr (class-name arg-string)
  "generates __repr__ for a python class."
  (let ((args (elpy-snippet-split-args arg-string)))
    (format "\"%s(%s)\".format(%s)"
            class-name
            (mapconcat
             (lambda (arg) (format "%s={}" (car arg)))
             args
             ", ")
            (mapconcat
             (lambda (arg) (format "repr(self.%s)" (car arg)))
             args
             ", "))))
