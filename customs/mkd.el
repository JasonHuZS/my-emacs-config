(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

(use-package flymd
  :ensure t)

(defun mkd-mode-setup ()
  (interactive)
  (developer-bundle)
  (auto-fill-mode))

(with-eval-after-load "markdown-mode"
  (add-to-list 'markdown-mode-hook 'mkd-mode-setup))
