(use-package javap-mode
  :ensure t)

(defun java-setup ()
  (interactive)
  (developer-bundle))

(defun javap-setup ()
  (developer-bundle)
  (javap-mode))

(add-hook 'java-mode-hook 'java-setup)

(add-hook 'javap-mode-hook 'javap-setup)

(add-to-list 'auto-mode-alist '("\\.j\\'" . javap-mode))
