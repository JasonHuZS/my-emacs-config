;; (load-and-log (from-emacsd "pulls/PG/generic/proof-site"))

(use-package proof-general
  :ensure t
  :config
  (setq proof-three-window-mode-policy 'hybrid))

(use-package company-coq
  :ensure t
  :config
  (setq company-coq-live-on-the-edge t)
  (setq company-coq-features/prettify-symbols-in-terminals t))

(with-eval-after-load 'coq
  (defvar company-coq-term-doc-history nil
    "the history list for company-coq-term-doc")

  (defun company-coq-term-doc (term)
    "company-coq-doc but allow interactive input"
    (interactive
     (list (completing-read "Term to query: " nil nil nil nil
                            'company-coq-term-doc-history
                            (current-word))))
    (when
        (company-coq-doc term (list company-coq-doc-cmd
                                    company-coq-def-cmd company-coq-tactic-def-cmd))))

  (defun redef-company-coq-doc (name cmds)
    (interactive (list (company-coq-symbol-at-point-with-error)
                       (list company-coq-doc-cmd company-coq-def-cmd
                             company-coq-tactic-def-cmd)))
    (add-to-history 'company-coq-term-doc-history name)
    (company-coq-doc name cmds))

  (defun redef-proof-minibuffer-cmd (cmd)
    "redefining proof-minibuffer-cmd"
    (interactive
     (list (completing-read "Command: " nil nil nil nil
                            'proof-minibuffer-history
                            (if (and current-prefix-arg (region-active-p))
                                (replace-regexp-in-string
                                 "[ \t\n]+" " "
                                 (buffer-substring (region-beginning) (region-end)))))))
    (proof-minibuffer-cmd cmd))


  (defun redef-proof-find-theorems (arg)
    "redefining proof-find-theorems"
    (interactive
     (list (format proof-find-theorems-command
                   (completing-read "Find theorems containing: "
                                    nil nil nil nil
                                    'proof-find-theorems-history))))
    (proof-find-theorems arg))

  (defvar coq-notation-locate-history nil
    "the history list for coq-notation-locate")

  (defvar coq-notation-locate-command "Locate \"%s\"."
    "the command for coq-notation-locate")

  (defun coq-notation-locate (arg)
    "Locate the notation."
    (interactive
     (list (completing-read "Locate: " nil nil nil nil
                            'coq-notation-locate-history)))
    ;; (setq coq-notation-locate-history
    ;;       (cons arg (remove arg coq-notation-locate-history)))
    (proof-shell-invisible-command (format coq-notation-locate-command
                                           arg)))

  (defun coq-do-set-unset (do args setcmd unsetcmd
                              &optional postformatcmd tescmd)
    (let* ((postform (if (eq postformatcmd nil) 'identity postformatcmd tescmd)))
      (coq-command-with-set-unset setcmd (concat do " " args) unsetcmd postformatcmd)))

  (defun coq-do (do args &optional postformatcmd)
    (let* ((postform (if (eq postformatcmd nil) 'identity postformatcmd)))
      (proof-shell-invisible-command
       (format (concat do " %s. ") (funcall postform args)))))

  (defun coq-do-show-all (do args &optional dontguess postformatcmd)
    (coq-do-set-unset do args
                      "Set Printing All"
                      "Unset Printing All"
                      postformatcmd
                      "Test Printing All"))

  (defvar coq-expr-history nil
    "the history list for coq-Check and its relatives.")

  (defun coq-Check (withprintingall args)
    "This function has been redefined.

Ask for a term and print its type.
With flag Printing All if some prefix arg is given (C-u)."
    (interactive
     (list current-prefix-arg
           (completing-read "Check: " nil nil nil nil
                            'coq-expr-history
                            (select-region-if-active))))
    (if withprintingall
        (coq-do-show-all "Check" args)
      (coq-do "Check" args)))


  (defun proof-query-identifier (string)
    "Query the prover about the identifier STRING.
If called interactively, STRING defaults to the current word near point."
    (interactive
     (list
      (completing-read "Query identifier: "
                       nil nil nil
                       (select-region-if-active 'current-word)
                       'proof-query-identifier-history)))
    (if string (pg-identifier-query string)))

  
  (defun coq-Check-show-all (args)
    "Ask for a term and print its type."
    (interactive
     (list current-prefix-arg
           (completing-read "Check: " nil nil nil nil
                            'coq-expr-history
                            (select-region-if-active))))
    (coq-do-show-all "Check" args))

  (defun coq-eval-expr (strat expr)
    "evaluate an expression."
    (interactive
     (let* ((default-strat "compute")
            (strat (if current-prefix-arg
                       (completing-read "Eval {strategy} in "
                                        (list "compute" "simpl"
                                              "vm_compute" "native_compute"
                                              "cbv" "lazy")
                                        nil 'confirm nil
                                        nil
                                        default-strat)
                     default-strat)))
       (list strat
             (completing-read (format "Eval %s in " strat)
                              nil nil nil nil
                              'coq-expr-history
                              (select-region-if-active)))))
    (coq-do (format "Eval %s in" strat) expr))


  (defvar coq-print-history nil)

  (defun coq-Print (withprintingall args)
    "This function has been redefined.

Ask for an ident and print the corresponding term.
With flag Printing All if some prefix arg is given (C-u)."
    (interactive
     (list current-prefix-arg
           (completing-read "Print " nil nil nil nil
                            'coq-print-history
                            (select-region-if-active 'current-word))))
    (if withprintingall
        (coq-do-show-all "Print" args)
      (coq-do "Print" args)))

  (defvar coq-match-history nil)
  (defun redef-company-coq-insert-match-construct (type)
    "This function redefines company-coq-insert-match-construct"
    (interactive
     (list (completing-read "Type to destruct (e.g. nat, bool, list, …): "
                            nil nil nil nil
                            'coq-match-history)))
    (company-coq-insert-match-construct type))

  (proof-definvisible coq-set-LtacDebug "Set Ltac Debug. ")
  (proof-definvisible coq-unset-LtacDebug "Unset Ltac Debug. ")

  (defun coq-debug-eauto-output--transform (flag)
    (goto-char (line-beginning-position))
    (let ((line (buffer-substring-no-properties
                 (line-beginning-position)
                 (line-end-position))))
      (pcase line
        ("" nil)
        ("Debug: (* debug eauto: *)"
         (delete-region (line-beginning-position) (line-end-position))
         (if flag (insert "}\n"))
         (insert "{")
         t)
        (_
         (if (re-search-forward "\\(.*?depth=[[:digit:]]+\\)[[:space:]]+\\(.*\\)"
                                (line-end-position)
                                t)
             (replace-match "  (* \\1 *) \\2.")
           (insert "  (* ")
           (goto-char (line-end-position))
           (insert " *)"))
         nil))))

  (defun coq-debug-eauto-output-proc (arg)
    "transform `debug eauto' into runnable output.

put prefix key if one wants to yank it immediately."
    (interactive "P")
    (if proof-response-buffer
        (progn
          (with-temp-buffer
            (let ((tmp (current-buffer))
                  (open nil))
              (with-current-buffer proof-response-buffer
                (copy-to-buffer tmp 1 (point-max)))
              (goto-char 1)
              (while (not (= (point) (point-max)))
                (setq open (or (coq-debug-eauto-output--transform open)
                               open))
                (forward-line 1))
              
              (if open (insert "}"))
              (copy-region-as-kill 1 (point-max))))

          (if arg (yank)))
      (error "there is no proof general response buffer!")))

  )

(defun coq-mode-setup ()
  (interactive)
  (developer-bundle)
  (flycheck-mode 0)
  (sp-local-pair 'coq-mode "`" nil :actions nil)
  
  (bind-keys :map (current-local-map)
             ("C-c RET"     . proof-goto-point)
             ("C-c ,"       . company-coq-occur)
             ("ESC <f12>"   . company-coq-toggle-definition-overlay)
             ("C-c \\"      . company-coq-insert-match-rule-simple)
             ("C-c |"       . company-coq-insert-match-rule-complex)
             ("C-c d"       . company-coq-term-doc)
             ("C-c C-v"     . redef-proof-minibuffer-cmd)
             ("C-c C-f"     . redef-proof-find-theorems)
             ("C-c ;"       . coq-notation-locate)
             :map company-coq-map
             ("C-c C-e"     . coq-eval-expr)
             ("C-c C-d"     . redef-company-coq-doc)
             ("C-c C-a RET" . redef-company-coq-insert-match-construct)
             :map coq-mode-map
             ("C-c C-a M-l" . coq-set-LtacDebug)
             ("C-c C-a l"   . coq-unset-LtacDebug)
             ;; :map coq-mode-map
             ;; ("C-c C-a C-c" . redef-coq-Check)
             ;; ("C-c C-a C-p" . redef-coq-Print)))
             ))

(setq overlay-arrow-string "")

(add-hook 'coq-mode-hook 'company-coq-mode)
(add-hook 'coq-mode-hook 'coq-mode-setup)
