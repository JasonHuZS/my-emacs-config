(use-package company-math
  :ensure t)

(use-package latex-extra
  :ensure t)

(use-package company-auctex
  :ensure t)

(use-package company-bibtex
  :ensure t)

(use-package latex-preview-pane
  :ensure t
  :config
  (setq pdf-latex-command "pdflatex")
  (make-variable-buffer-local 'pdf-latex-command))

(customize-set-variable 'doc-view-continuous t)

(defun latex-mode-setup ()
  (interactive)
  (company-auctex-init)
  (developer-bundle)
  (latex-preview-pane-mode)
  (auto-fill-mode)
  (flyspell-mode t)
  (sp-local-pair 'latex-mode "'" nil :actions nil)
  (setq-local company-backends
              (append '((company-math-symbols-latex company-latex-commands))
                      company-backends))
  (add-to-list 'company-backends 'company-bibtex)
  (let ((root (projectile-project-root)))
    (if root
        (if (file-exists-p (concat root ".use-xelatex"))
            (setq-local pdf-latex-command "xelatex")))))

(add-hook 'tex-mode-hook 'latex-mode-setup)
(add-hook 'TeX-mode-hook 'latex-mode-setup)

(defconst pdf-latex-preview-magic-file ".latex-preview"
  "the magic file contains the execute to execute to obtain a modifie latex preview file")

(defconst pdf-latex-preview-excluded ".preview-excluded"
  "the file that contains a list of relative names that should be excluded from extended preview feature")

(with-eval-after-load "latex-preview-pane"
  (defun lpp/invoke-pdf-latex-command-old ()
    (let ((buff (expand-file-name (lpp/buffer-file-name))) (default-directory (file-name-directory (expand-file-name (lpp/buffer-file-name)))))
      (if shell-escape-mode
	  (call-process pdf-latex-command nil "*pdflatex-buffer*" nil shell-escape-mode buff)
        (call-process pdf-latex-command nil "*pdflatex-buffer*" nil buff)
        )
      )
    )
  
  (defun lpp/invoke-pdf-latex-command ()
    (let* ((project-root (file-name-as-directory (projectile-project-root)))
           (magic-file (concat project-root pdf-latex-preview-magic-file))
           (excluded-file (concat project-root pdf-latex-preview-excluded))
           (excluded-list (if (f-exists-p excluded-file)
                              (with-temp-buffer (insert-file-contents excluded-file)
                                                (split-string (buffer-string) "\n" t))
                            nil)))
      (if (f-exists-p magic-file)
          (let* ((magic-content (with-temp-buffer (insert-file-contents magic-file) (s-trim (buffer-string))))
                 (magic-binary (concat project-root magic-content))
                 (buff (expand-file-name (lpp/buffer-file-name)))
                 (buff-rel (file-relative-name buff project-root)))
            (if (member buff-rel excluded-list)
                (lpp/invoke-pdf-latex-command-old)
              (call-process magic-binary nil "*pdflatex-buffer*" nil buff)))
        (lpp/invoke-pdf-latex-command-old))))
  )
