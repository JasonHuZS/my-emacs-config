(use-package racket-mode
  :ensure t)

(defun racket-setup ()
  (racket-unicode-input-method-enable))

(add-hook 'racket-mode-hook 'racket-setup)
(add-hook 'racket-repl-mode-hook 'racket-setup)
