(use-package elisp-def
  :ensure t
  :diminish elisp-def-mode)

(defun jump-to-function ()
  (interactive)
  (xref-push-marker-stack)
  (find-function (function-called-at-point)))

(defun elisp-setup ()
  "setup emacs lisp environment."
  (local-set-key (kbd "C-h C-f") 'find-function)
  (eldoc-mode t)
  (elisp-def-mode)
  (flycheck-mode t))

(with-eval-after-load 'flycheck
    (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(add-hook 'emacs-lisp-mode-hook 'elisp-setup)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)
