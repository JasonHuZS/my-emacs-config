(use-package tuareg
  :ensure t)

(use-package merlin
  :ensure t)

(use-package merlin-company
  :ensure t)

(use-package flycheck-ocaml
  :ensure t
  :config
  (setq merlin-error-after-save nil)
  (flycheck-ocaml-setup))

(use-package dune
  :ensure t)

(use-package merlin-eldoc
  :after merlin
  :ensure t
  :custom
  (eldoc-echo-area-use-multiline-p t) ; use multiple lines when necessary
  (merlin-eldoc-max-lines 8)          ; but not more than 8
  (merlin-eldoc-type-verbosity 'min)  ; don't display verbose types
  (merlin-eldoc-function-arguments nil) ; don't show function arguments
  (merlin-eldoc-doc nil)                ; don't show the documentation
  :bind (:map merlin-mode-map
              ("C-c m p" . merlin-eldoc-jump-to-prev-occurrence)
              ("C-c m n" . merlin-eldoc-jump-to-next-occurrence))
  :hook ((tuareg-mode reason-mode) . merlin-eldoc-setup))

;; (setenv "OPAMROOT" opam-root)

;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup (from-emacsd "opam-user-setup.el"))
;; ## end of OPAM user-setup addition for emacs / base ## keep this line

(with-eval-after-load 'company
  (add-to-list 'company-backends 'merlin-company-backend))

(with-eval-after-load 'tuareg
  (let ((opam-share (ignore-errors (car (process-lines "opam" "config" "var" "share")))))
    (when (and opam-share (file-directory-p opam-share))
      ;; Register Merlin
      (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))
      (autoload 'merlin-mode "merlin" nil t nil)
      ;; Automatically start it in OCaml buffers
      (add-hook 'tuareg-mode-hook 'merlin-mode t)
      (add-hook 'caml-mode-hook 'merlin-mode t)
      ;; Use opam switch to lookup ocamlmerlin binary
      (setq merlin-command 'opam)))

  (setq tuareg-indent-align-with-first-arg t)

  (defvar ocaml-align-symbols
    (list "="
          ":"
          "->"
          "|"
          "of"))

  (defvar ocaml-align-symbols-history
    nil)

  (defvar ocaml-align-region-symbols-history nil)
  
  (defun ocaml-search-sym (&optional syms)
    (search-sym (or syms ocaml-align-symbols)))

  (defun ocaml-search-sym-region (&optional syms re-space)
    (search-sym-region (or syms ocaml-align-symbols) re-space))
  
  (align-interactive-macro 'ocaml 'left)
  (align-interactive-macro 'ocaml 'right)
  (align-region-interactive-macro 'ocaml)

  (bind-keys :map tuareg-mode-map
             ("C-c a" . ocaml-align-sym-re-by-region-left)
             ("C-c s" . ocaml-align-sym-re-by-region-right)
             ("C-c d" . ocaml-align-region-interactive)
             ("M-."   . lsp-ui-peek-find-definitions)
             ("C-c r" . lsp-ui-peek-find-references))

  )

(with-eval-after-load 'merlin
  (bind-keys :map merlin-mode-map
             ("M-." . merlin-locate)
             ("M-," . merlin-pop-stack))
  )

(defun ocaml-mode-setup ()
  (interactive)
  (developer-bundle)
  (company-mode)
  (merlin-mode)
  (lsp)
  (prettify-symbols-mode))

(add-to-list 'auto-mode-alist '("\\.mll\\'" . tuareg-mode))

(add-hook 'tuareg-mode-hook 'ocaml-mode-setup)
