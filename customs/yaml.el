(use-package yaml-mode
  :ensure t
  :bind (:map yaml-mode-map
              ("C-m" . newline-and-indent)))


(use-package yaml-tomato
  :ensure t)

(use-package flycheck-yamllint
  :ensure t
  :defer t
  :init
  (progn
    (eval-after-load 'flycheck
      '(add-hook 'flycheck-mode-hook 'flycheck-yamllint-setup))))

(with-eval-after-load "yaml-mode"
  (add-to-list 'yaml-mode-hook 'developer-bundle))
