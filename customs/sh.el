(use-package company-shell
  :ensure t)

(defun sh-mode-setup ()
  (interactive)
  (add-to-list 'company-backends '(company-shell company-shell-env))
  (flycheck-mode t))

(add-hook 'sh-mode-hook 'sh-mode-setup)
