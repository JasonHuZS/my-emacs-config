(use-package dash)

(defconst aut-desc-keywords
  '("alphabet" "states" "initial" "finish" "transitions")
  "constants for dfa mode")

(defconst aut-desc-font-lock
  (let* ((components aut-desc-keywords))
    `((,(regexp-opt components 'words) . font-lock-function-name-face))))

(defconst aut-desc-mode-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?= "." table)
    table))


(defun aut-eq-line-offset ()
  "calculate the position of '=' when it's pressed."
  (+ 1 (-max (--map (length it) aut-desc-keywords))))


(defun aut-align-eq ()
  "align '=' to the proper position when pressed."
  (interactive)
  (indent-to-column (aut-eq-line-offset) 1)
  (self-insert-command 1)
  (insert " "))

(defun aut-indent-func ()
  (interactive)

  (defun iterate (begin)
    (goto-char begin)
    (let* ((end (line-end-position 1))
           (substr (buffer-substring-no-properties begin end)))
      (if (string-match-p (regexp-quote "=") substr)
          ; then we are in a definition line
          (let ((field (string-trim
                        (substring-no-properties
                         substr
                         0
                         (string-match (regexp-quote "=") substr)))))
            (string-equal field "transitions"))
        ; then we need to call recursively
        (if (/= begin (line-beginning-position 0))
            (iterate (line-beginning-position 0))))))

  (let ((found (save-excursion
                 (iterate (line-beginning-position 0)))))
    (if found
        (progn
          (indent-to-left-margin)
          (back-to-indentation)
          (indent-to-column (+ 2 (aut-eq-line-offset))))
      (indent-relative))))


;;;###autoload
(define-derived-mode aut-desc-mode prog-mode "Aut"
  "Major mode for Automata editing at University of Waterloo."
  :syntax-table aut-desc-mode-syntax-table
  (setq comment-start "")
  (setq comment-end "")
  
  (setq font-lock-defaults '(aut-desc-font-lock))
  (setq indent-line-function 'aut-indent-func)
  (bind-keys :map aut-desc-mode-map
             ("=" . aut-align-eq)))


(add-to-list 'auto-mode-alist '("\\.dfa-desc\\'" . aut-desc-mode))
(add-to-list 'auto-mode-alist '("\\.nfa-desc\\'" . aut-desc-mode))

(defun aut-desc-mode-setup ())

(add-hook 'aut-desc-mode-hook 'aut-desc-mode-setup)
