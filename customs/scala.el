;; (use-package ensime
;;   :ensure t
;;   :pin melpa)

(use-package sbt-mode
  :ensure t
  :pin melpa)

(use-package scala-mode
  :ensure t
  :pin melpa)

