(use-package cmake-mode
  :ensure t)

(use-package cmake-font-lock
  :ensure t)

(autoload 'cmake-font-lock-activate "cmake-font-lock" nil t)
(add-hook 'cmake-mode-hook 'cmake-font-lock-activate)
