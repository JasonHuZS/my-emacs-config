(use-package redprl)

(defun redprl-setup ()
  (sp-local-pair 'redprl-mode "`" nil :actions nil))

(add-hook `redprl-mode-hook 'redprl-setup)
