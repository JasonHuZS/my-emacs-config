(use-package haskell-mode
  :ensure t
  :config
  (custom-set-variables '(haskell-tags-on-save t))

  (custom-set-variables
   '(haskell-process-suggest-remove-import-lines t)
   '(haskell-process-auto-import-loaded-modules t)
   '(haskell-process-log t))
  (custom-set-variables '(haskell-process-type 'cabal-repl))
  :bind (:map haskell-mode-map
              ("C-c C-l"     . haskell-process-load-or-reload)
              ("C-c C-z"     . haskell-interactive-switch)
              ("C-c C-n C-t" . haskell-process-do-type)
              ("C-c C-n C-i" . haskell-process-do-info)
              ("C-c C-n C-c" . haskell-process-cabal-build)
              ("C-c C-n c"   . haskell-process-cabal)
              ("C-c C-o"     . haskell-compile)
              ("M-."         . lsp-ui-peek-find-definitions)))

(use-package haskell-cabal
  :bind (:map haskell-cabal-mode-map
              ("C-c C-z" . haskell-interactive-switch)
              ("C-c C-k" . haskell-interactive-mode-clear)
              ("C-c C-c" . haskell-process-cabal-build)
              ("C-c c"   . haskell-process-cabal)))

(use-package company-cabal
  :ensure t)

(use-package hindent
  :ensure t)

(use-package lsp-haskell
  :ensure t
  :hook (haskell-mode . lsp)
  :config
  (setq lsp-haskell-process-path-hie "hie-wrapper"))

(use-package flycheck-haskell
  :ensure t)

;; (use-package shm
;;   :ensure t
;;   :bind (:map shm-map
;;               ("C-c C-s" . shm/case-split)))

(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))

(defun haskell-mode-setup ()
  (interactive)
  (haskell-indent-mode t)
  ;; (interactive-haskell-mode t)
  (haskell-decl-scan-mode t)
  (turn-on-haskell-indentation)
  ;; (structured-haskell-mode t)
  (flycheck-mode t)
  (add-to-list 'company-backends 'company-cabal))

(add-hook 'haskell-mode-hook 'haskell-mode-setup)
