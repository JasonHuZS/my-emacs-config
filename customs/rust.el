(defun toml-mode-setup ()
  (interactive)
  (developer-bundle))

(use-package toml-mode
  :ensure t
  :config
  (add-hook 'toml-mode-hook 'toml-mode-setup))

(use-package racer
  :ensure t
  :config
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (let ((sys-path (shell-command-to-string "rustc --print sysroot")))
    (setq racer-rust-src-path (--reduce-from (expand-file-name it acc) (string-trim sys-path) '("lib" "rustlib" "src" "rust" "src"))))
  (add-hook 'racer-mode-hook #'company-mode))

(defun rust-mode-setup ()
  (interactive)
  (developer-bundle)
  (setq indent-tabs-mode nil)
  (racer-mode))

(use-package rust-mode
  :ensure t
  :hook (rust-mode . lsp)
  :config
  (add-hook 'rust-mode-hook 'rust-mode-setup)
  (setq rust-format-on-save t)
  :bind (:map rust-mode-map
              ("C-c c" . rust-run)
              ("M-."   . lsp-ui-peek-find-definitions)
              ("C-c r" . lsp-ui-peek-find-references)
         :map racer-mode-map
              ("M-."   . lsp-ui-peek-find-definitions))
  )

;; Add keybindings for interacting with Cargo
(use-package cargo
  :ensure t
  :hook (rust-mode . cargo-minor-mode))

(use-package flycheck-rust
  :ensure t
  :config (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

