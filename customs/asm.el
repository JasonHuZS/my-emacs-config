(use-package dash)

(defun mips-stack-snippet (text mode)
    (let* ((lex (split-string (replace-regexp-in-string "," " " text)))
           (nums (cons 31
                       (remove-duplicates
                        (--filter (/= it 0)
                                  (--map (string-to-number it) lex)))))
           (size (length nums))
           (sname (symbol-name mode))
           (asm (--map-indexed
                 (format "%s  $%d, -%d($30)" sname it (* 4 (+ 1 it-index)))
                 nums)))
    (cond
     ((equal 'sw mode)
      (concat (s-join "\n" asm)
              "\n"
              (s-join "\n"
                      (list "lis $31"
                            (format ".word -%d" (* 4 size))
                            "add $30, $30, $31"))))
     ((equal 'lw mode)
      (concat (s-join "\n"
                      (list "lis $31"
                            (format ".word %d" (* 4 size))
                            "add $30, $30, $31"))
              "\n"
              (s-join "\n" (reverse asm))
              "\n"
              "jr  $31"))
     (t (error "%s is not a valid mode" mode)))))
