(load-file (let ((coding-system-for-read 'utf-8))
             (shell-command-to-string "agda-mode locate")))

(with-eval-after-load 'agda2-mode
  (use-package smartparens)
  (use-package alignments)
  
  (sp-with-modes 'agda2-mode
    (sp-local-pair "⟨" "⟩")
    (sp-local-pair "⦃" "⦄")
    (sp-local-pair "{-" "-}")
    (sp-local-pair "{-#" "#-}")
    (sp-local-pair "⟪" "⟫")
    (sp-local-pair "⦇" "⦈")
    (sp-local-pair "〈" "〉")
    (sp-local-pair "⟦" "⟧"))

  (defvar agda-align-symbols
    (list "="
          ":"
          "with\\|rewrite\\||"
          "→"
          "∶"
          "[^[:space:]]\\{1,2\\}⟨\\|∎"
          ";\\|{\\|}\\|record"))

  (defvar agda-align-symbols-history
    nil)

  (defun agda-search-sym (&optional syms)
    (search-sym (or syms agda-align-symbols)))

  (defun agda-search-sym-region (&optional syms re-space)
    (search-sym-region (or syms agda-align-symbols) re-space))
  
  (align-interactive-macro 'agda 'left)
  (align-interactive-macro 'agda 'right)

  (defvar agda-align-region-symbols-history nil)

  (align-region-interactive-macro 'agda)
  
  (bind-keys :map agda2-mode-map
             ("C-c a" . agda-align-sym-re-by-region-left)
             ("C-c s" . agda-align-sym-re-by-region-right)
             ("C-c d" . agda-align-region-interactive)
             ("C-c RET" . agda2-goal-and-context))

  (defun agda2-compile (backend)
    "Compile the current module.

The variable `agda2-backend' determines which backend is used."
    (interactive
     (list (if (or current-prefix-arg (equal agda2-backend ""))
               (completing-read "Backend: " agda2-backends
                                nil nil nil nil nil
                                'inherit-input-method)
             agda2-backend)))
    (setq backend (cond ((equal backend "MAlonzo")       "GHC")
                        ((equal backend "MAlonzoNoMain") "GHCNoMain")
                        (t backend)))
    (agda2-go 'save t t t "Cmd_compile"
              backend
              (agda2-string-quote (buffer-file-name))
              (agda2-list-quote agda2-program-args)))

  (defconst agda--separator "—")
  
  (defun agda--fetch-info-buffer ()
    "fetch the content from info buffer if it exists."
    (if agda2-info-buffer
        (let ((lines (with-current-buffer agda2-info-buffer
                       (split-string (buffer-string) "\n" t)))
              (res nil))

          (--each lines
            (cond
             ((string-prefix-p "Goal: " it) (push `(goal . (,(substring-no-properties it 6))) res))
             ((string-prefix-p "Have: " it) (push `(have . (,(substring-no-properties it 6))) res))
             ((--all? (string= agda--separator it) (split-string it "" t))
              (push '(rest) res))
             (t (if res
                    (let ((lab (caar res))
                          (ls (cdar res)))
                      (setcar res `(,lab ,(substring-no-properties it) . ,ls)))))))

          (--map (let ((lab (car it))
                       (ls (cdr it)))
                   `(,lab . ,(reverse ls))) res))))

  (defmacro agda--fetch-info-macro (key)
    `(defun ,(intern (format "agda-fetch-%s" (cadr key))) (do-yank processor separator)
       ,(format "put %s string into kill ring. C-u will also yank it to the current buffer." (cadr key))
       (interactive (pcase (prefix-pressed)
                      (`0 (list nil 'identity "\n"))
                      (`1 (list t 'string-trim " "))
                      (_ (list t 'identity "\n"))))
       (let ((info (agda--fetch-info-buffer)))
         (if info
             (let ((value (or (alist-get ,key info)
                              (error ,(format "could not find %s from AgdaInfo buffer!" (cadr key))))))
               (kill-new (mapconcat (lambda (x) (funcall processor x)) value separator))
               (message ,(format "%s has been put into kill ring." (cadr key)))
               (if do-yank (yank)))
           (error "AgdaInfo buffer cannot be parsed or found")))))

  (agda--fetch-info-macro 'goal)
  (agda--fetch-info-macro 'have)
  (bind-keys :map agda2-mode-map
             ("C-c q" . agda-fetch-goal)
             ("C-c e" . agda-fetch-have))
  
  (setq agda2-backend "GHC"))


(with-eval-after-load "agda2-highlight"
  (set-face-foreground 'agda2-highlight-function-face "PowderBlue")
  (set-face-foreground 'agda2-highlight-datatype-face "GreenYellow")
  (set-face-foreground 'agda2-highlight-postulate-face "LightGreen")
  (set-face-foreground 'agda2-highlight-primitive-type-face "Gold")
  (set-face-foreground 'agda2-highlight-record-face "NavajoWhite")
  (set-face-foreground 'agda2-highlight-inductive-constructor-face "SandyBrown")
  (set-face-foreground 'agda2-highlight-keyword-face "Khaki")
  (set-face-foreground 'agda2-highlight-module-face "Orchid")
  (set-face-foreground 'agda2-highlight-number-face "Wheat")
  (set-face-foreground 'agda2-highlight-primitive-face "Chartreuse")

  (safe-set-face 'set-face-background 'agda2-highlight-catchall-clause-face "DarkSlateGray")
  (safe-set-face 'set-face-background 'agda2-highlight-error-face "DarkSlateGray")
  (safe-set-face 'set-face-background 'agda2-highlight-unsolved-meta-face "DarkSlateGray")
  (safe-set-face 'set-face-background 'agda2-highlight-unsolved-constraint-face "SteelBlue")
  (safe-set-face 'set-face-foreground 'agda2-highlight-unsolved-constraint-face "WhiteSmoke"))

