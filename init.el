(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("90a6f96a4665a6a56e36dec873a15cbedf761c51ec08dd993d6604e32dd45940" "7922b14d8971cce37ddb5e487dbc18da5444c47f766178e5a4e72f90437c0711" default))
 '(doc-view-continuous t)
 '(elpy-syntax-check-command "flake8")
 '(fci-rule-color "#3a3a3a")
 '(flycheck-checker-error-threshold 600)
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-process-type 'cabal-repl)
 '(haskell-tags-on-save t)
 '(package-selected-packages
   '(merlin-company flycheck-yamllint yaml-tomato yaml-mode company-shell scala-mode sbt-mode flycheck-rust cargo racer toml-mode racket-mode python-environment py-autopep8 elpy merlin-eldoc dune flycheck-ocaml merlin tuareg flymd latex-preview-pane company-bibtex company-auctex latex-extra json-mode javap-mode web-mode flycheck-haskell lsp-haskell hindent company-cabal haskell-mode fstar-mode elisp-def company-coq proof-general cmake-font-lock cmake-mode ccls function-args clean-aindent-mode company-c-headers bison-mode boogie-friends yasnippet-snippets ws-butler volatile-highlights use-package undo-tree treemacs-projectile treemacs-magit treemacs-icons-dired switch-window swap-buffers smartparens rainbow-delimiters powerline popup-imenu origami mmm-mode material-theme magit-popup lsp-ui lsp-treemacs iedit helm-projectile goto-chg git-gutter-fringe free-keys fiplr fic-mode expand-region dtrt-indent diminish company-flx avy-flycheck anzu))
 '(safe-local-variable-values
   '((eval let
           ((default-directory
              (locate-dominating-file buffer-file-name ".dir-locals.el")))
           (make-local-variable 'coq-prog-name)
           (setq coq-prog-name
                 (expand-file-name "../hoqtop")))))
 '(shell-escape-mode "-shell-escape")
 '(web-mode-enable-auto-expanding t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'package)

(add-to-list
 'package-archives
 ;; '("melpa" . "http://stable.melpa.org/packages/") ; many packages won't show if using stable
 '("melpa" . "http://melpa.org/packages/"))

; "http://melpa.milkbox.net/packages/")))

(add-to-list 'package-archives
             '("elpy" . "http://jorgenschaefer.github.io/packages/"))

(package-initialize)

(package-refresh-contents)

(when (not (require 'use-package nil 'noerror))
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

;; BASIC CUSTOMIZATION

;; --------------------------------------

(defconst emacsd "~/.emacs.d"
  "the path to the emacs directory")

(defun from-emacsd (p) (concat emacsd "/" p))

(defconst manual-packages (from-emacsd "manual")
  "the path to the manually installed packages.")

(defun load-and-log (path)
  (message "loading from path %s..." path)
  (load path))

;; load all packages and customized util functions

(mapc 'load-and-log (file-expand-wildcards (from-emacsd "inits/*.el")))
